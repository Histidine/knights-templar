package data.scripts.shipsystems.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.hullmods.TEM_LatticeShield;
import java.util.ArrayList;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_AegisShieldAI implements ShipSystemAIScript {

    private static final float SECONDS_TO_LOOK_AHEAD = 2f;

    private CombatEngineAPI engine;

    private final CollectionUtils.CollectionFilter<DamagingProjectileAPI> filterMisses = new CollectionUtils.CollectionFilter<DamagingProjectileAPI>() {
        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner() && (!(proj instanceof MissileAPI) || !((MissileAPI) proj).isFizzling())) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare()) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(
                                                                                SECONDS_TO_LOOK_AHEAD), null),
                                               ship.getLocation(), ship.getCollisionRadius() + 50f) &&
                    Math.abs(MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f);
        }
    };

    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        if (ship.getSystem().isActive()) {
            flags.setFlag(AIFlags.DO_NOT_VENT);
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting() || ship.getSystem().isActive()) {
                return;
            }

            boolean shouldUseSystem = false;

            List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(500);
            for (DamagingProjectileAPI tmp : Global.getCombatEngine().getProjectiles()) {
                if (MathUtils.isWithinRange(tmp.getLocation(), ship.getLocation(), ship.getCollisionRadius() * 5f)) {
                    nearbyThreats.add(tmp);
                }
            }
            nearbyThreats = CollectionUtils.filter(nearbyThreats, filterMisses);
            List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, ship.getCollisionRadius() * 2.5f);
            for (MissileAPI missile : nearbyMissiles) {
                if (!missile.getEngineController().isTurningLeft() && !missile.getEngineController().isTurningRight()) {
                    continue;
                }

                nearbyThreats.add(missile);
            }

            float decisionLevel = 0f;
            float damageReduction = TEM_LatticeShield.shieldLevel(ship);
            for (DamagingProjectileAPI threat : nearbyThreats) {
                if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                    decisionLevel += Math.pow((float) Math.sqrt(1f - damageReduction) * (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 400f, 1.3f);
                } else {
                    decisionLevel += Math.pow((float) Math.sqrt(1f - damageReduction) * (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 100f, 1.3f);
                }
            }
            List<BeamAPI> nearbyBeams = Global.getCombatEngine().getBeams();
            for (BeamAPI beam : nearbyBeams) {
                if (beam.getDamageTarget() == ship) {
                    float damage;
                    float emp = beam.getWeapon().getDerivedStats().getEmpPerSecond();
                    if (beam.getWeapon().isBurstBeam()) {
                        damage = beam.getWeapon().getDerivedStats().getBurstDamage() / beam.getWeapon().getDerivedStats().getBurstFireDuration();
                    } else {
                        damage = beam.getWeapon().getDerivedStats().getDps();
                    }
                    decisionLevel += Math.pow((float) Math.sqrt(1f - damageReduction) * ((damage * ((beam.getWeapon().getDamageType() ==
                                                                                                     DamageType.FRAGMENTATION) ? 0.25f : 1f)) + (0.25f * emp)) /
                    100f, 1.3f);
                }
            }

            decisionLevel *= (ship.getFluxTracker().getCurrFlux() - 2f * ship.getFluxTracker().getHardFlux()) / ship.getFluxTracker().getMaxFlux() + 1f;
            decisionLevel *= (float) Math.sqrt(2f - ship.getHullLevel());
            if (flags.hasFlag(AIFlags.PURSUING)) {
                decisionLevel *= 0.5f;
            }
            if (flags.hasFlag(AIFlags.RUN_QUICKLY)) {
                decisionLevel *= 1.5;
            } else if (flags.hasFlag(AIFlags.BACK_OFF) || flags.hasFlag(AIFlags.BACK_OFF_MIN_RANGE) || flags.hasFlag(AIFlags.BACKING_OFF)) {
                decisionLevel *= 1.25;
            }
            if (!flags.hasFlag(AIFlags.HAS_INCOMING_DAMAGE)) {
                decisionLevel *= 0.75f;
            }

            if (decisionLevel >= (float) Math.sqrt(ship.getMaxHitpoints() / 40f)) {
                shouldUseSystem = true;
            }

            if ((ship.getSystem().isActive() && !shouldUseSystem) || (!ship.getSystem().isActive() && shouldUseSystem)) {
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
