package data.scripts.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_HeavySchismDriveStats extends BaseShipSystemScript {

    private static final Color COLOR1 = new Color(246, 238, 139);
    private static final Color COLOR2 = new Color(246, 238, 139, 100);
    private static final Color COLOR3 = new Color(246, 238, 139, 150);
    private static final Color COLOR4 = new Color(255, 240, 135);

    private static final String DATA_KEY = "TEM_HeavySchismDrive";

    private static final Vector2f ZERO = new Vector2f();

    public static float cooldownTime(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        if (coolingDown.containsKey(ship)) {
            return Global.getCombatEngine().getTotalElapsedTime(false) - coolingDown.get(ship);
        } else {
            return 0f;
        }
    }

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> acting = localData.acting;

        if (acting.containsKey(ship)) {
            return acting.get(ship);
        } else {
            return 0f;
        }
    }

    public static void removeFromCooldown(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        if (coolingDown.containsKey(ship)) {
            coolingDown.remove(ship);
        }
    }

    private final IntervalUtil interval = new IntervalUtil(0.033f, 0.033f);
    private StandardLight light = null;
    private boolean started = false;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }

        float amount = engine.getElapsedInLastFrame();

        interval.advance(amount);
        boolean intervalElapsed = interval.intervalElapsed();

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Float> acting = localData.acting;
        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (ship != null) {
            if (effectLevel > 0f) {
                acting.put(ship, effectLevel);
            }

            if (state == State.IN && effectLevel > 0f) {
                if (light == null) {
                    light = new StandardLight(ZERO, ZERO, ZERO, ship);
                    light.setColor(COLOR1);
                    light.setIntensity(2.75f);
                    light.setSize(650f);
                    light.fadeIn(0.3f);
                    LightShader.addLight(light);
                }

                if (!started) {
                    started = true;
                    coolingDown.put(ship, engine.getTotalElapsedTime(false));
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float speed = ship.getVelocity().length();
                if (speed <= 2f) {
                    ship.getVelocity().set(VectorUtils.getDirectionalVector(ship.getLocation(), ship.getMouseTarget()));
                }
                if (speed < 1000f) {
                    ship.getVelocity().normalise();
                    ship.getVelocity().scale(Math.max(ship.getMutableStats().getMaxSpeed().getModifiedValue(), speed + amount * 1500f));
                }
            } else if (state == State.ACTIVE) {
                if (light != null) {
                    light.setIntensity(2.5f + (float) Math.random() * 0.5f);
                    light.setSize(600f + (float) Math.random() * 100f);
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float speed = ship.getVelocity().length();
                if (speed < 1000f) {
                    if (speed < 1f) {
                        ship.getVelocity().set(MathUtils.getPointOnCircumference(null, 1f, ship.getFacing()));
                    }
                    ship.getVelocity().normalise();
                    ship.getVelocity().scale(Math.max(ship.getMutableStats().getMaxSpeed().getModifiedValue(), speed + amount * 1500f));
                }

                if (intervalElapsed) {
                    Vector2f point1a = new Vector2f(109f, 72f);
                    VectorUtils.rotate(point1a, ship.getFacing(), point1a);
                    Vector2f.add(point1a, ship.getLocation(), point1a);
                    Vector2f point1b = new Vector2f(-100f, 0f);
                    VectorUtils.rotate(point1b,
                            VectorUtils.getFacing(ship.getVelocity()) + (float) Math.random() * 30f
                            - 15f, point1b);
                    Vector2f.add(point1a, point1b, point1b);
                    Vector2f point2a = new Vector2f(109f, -72f);
                    VectorUtils.rotate(point2a, ship.getFacing(), point2a);
                    Vector2f.add(point2a, ship.getLocation(), point2a);
                    Vector2f point2b = new Vector2f(-100f, 0f);
                    VectorUtils.rotate(point2b,
                            VectorUtils.getFacing(ship.getVelocity()) + (float) Math.random() * 30f
                            - 15f, point2b);
                    Vector2f.add(point2a, point2b, point2b);
                    Vector2f point3a = new Vector2f(-11f, 80f);
                    VectorUtils.rotate(point3a, ship.getFacing(), point3a);
                    Vector2f.add(point3a, ship.getLocation(), point3a);
                    Vector2f point3b = new Vector2f(-100f, 0f);
                    VectorUtils.rotate(point3b,
                            VectorUtils.getFacing(ship.getVelocity()) + (float) Math.random() * 30f
                            - 15f, point3b);
                    Vector2f.add(point3a, point3b, point3b);
                    Vector2f point4a = new Vector2f(-11f, -80f);
                    VectorUtils.rotate(point4a, ship.getFacing(), point4a);
                    Vector2f.add(point4a, ship.getLocation(), point4a);
                    Vector2f point4b = new Vector2f(-100f, 0f);
                    VectorUtils.rotate(point4b,
                            VectorUtils.getFacing(ship.getVelocity()) + (float) Math.random() * 30f
                            - 15f, point4b);
                    Vector2f.add(point4a, point4b, point4b);

                    Global.getCombatEngine().spawnEmpArc(ship, point1a, new SimpleEntity(point1a), new SimpleEntity(
                            point1b), DamageType.ENERGY, 0f, 0f, 1000f, null,
                            30f + (float) Math.random() * 15f, COLOR2, COLOR3);
                    Global.getCombatEngine().spawnEmpArc(ship, point2a, new SimpleEntity(point2a), new SimpleEntity(
                            point2b), DamageType.ENERGY, 0f, 0f, 1000f, null,
                            30f + (float) Math.random() * 15f, COLOR2, COLOR3);
                    Global.getCombatEngine().spawnEmpArc(ship, point3a, new SimpleEntity(point3a), new SimpleEntity(
                            point3b), DamageType.ENERGY, 0f, 0f, 1000f, null,
                            30f + (float) Math.random() * 15f, COLOR2, COLOR3);
                    Global.getCombatEngine().spawnEmpArc(ship, point4a, new SimpleEntity(point4a), new SimpleEntity(
                            point4b), DamageType.ENERGY, 0f, 0f, 1000f, null,
                            30f + (float) Math.random() * 15f, COLOR2, COLOR3);

                    float shipRadius = TEM_Util.effectiveRadius(ship);

                    List<ShipAPI> targets = CombatUtils.getShipsWithinRange(ship.getLocation(),
                            ship.getCollisionRadius());
                    for (ShipAPI target : targets) {
                        if (target != ship && target.getCollisionClass() != CollisionClass.NONE) {
                            Vector2f damagePoint;
                            int checker = 10;
                            while (true) {
                                damagePoint
                                        = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                                if (CollisionUtils.isPointWithinBounds(damagePoint, target) || checker <= 0) {
                                    break;
                                }
                                checker--;
                            }
                            if (checker <= 0) {
                                damagePoint = CollisionUtils.getCollisionPoint(ship.getLocation(), target.getLocation(),
                                        target);
                                if (damagePoint == null) {
                                    continue;
                                }
                            }
                            float damage = interval.getIntervalDuration() * 15000f;
                            float emp = interval.getIntervalDuration() * 20000f;
                            if (target.getOwner() == ship.getOwner()) {
                                damage *= 0.5f;
                            }
                            Global.getCombatEngine().applyDamage(target, damagePoint, damage, DamageType.ENERGY, emp,
                                    false, false, ship);
                            Global.getSoundPlayer().playSound("tem_heavyschismdrive_impact", 1f
                                    + (float) Math.random() * 0.1f,
                                    0.4f + (float) Math.random() * 0.2f,
                                    ship.getLocation(), target.getVelocity());
                            Global.getCombatEngine().addHitParticle(ship.getLocation(), ZERO,
                                    shipRadius * 3f, 0.2f, 0.4f, COLOR4);
                        }
                    }
                }
            } else {
                if (light != null) {
                    light.fadeOut(0.6f);
                    light = null;
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float speed = ship.getVelocity().length();
                if (speed > ship.getMutableStats().getMaxSpeed().getModifiedValue()) {
                    ship.getVelocity().normalise();
                    ship.getVelocity().scale(Math.max(ship.getMutableStats().getMaxSpeed().getModifiedValue(), speed - amount * 1500f));
                }
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("ripping through space", false);
        }
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (light != null) {
            light.fadeOut(0.6f);
            light = null;
        }
        started = false;
        if (ship != null) {
            if (!Global.getCombatEngine().getCustomData().containsKey(DATA_KEY)) {
                Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
            }
            final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
            if (localData != null) {
                final Map<ShipAPI, Float> acting = localData.acting;

                acting.remove(ship);
            }
            ship.setCollisionClass(CollisionClass.SHIP);
            float speed = ship.getVelocity().length();
            if (speed > ship.getMutableStats().getMaxSpeed().getModifiedValue()) {
                ship.getVelocity().normalise();
                ship.getVelocity().scale(ship.getMutableStats().getMaxSpeed().getModifiedValue());
            }
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Float> acting = new HashMap<>(5);
        final Map<ShipAPI, Float> coolingDown = new HashMap<>(5);
    }
}
