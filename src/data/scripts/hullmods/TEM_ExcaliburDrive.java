package data.scripts.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.FighterWingAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.DModManager;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import data.scripts.everyframe.TEM_AlmaceBurstPlugin;
import data.scripts.everyframe.TEM_BlockedHullmodDisplayScript;
import data.scripts.everyframe.TEM_PriwenBurstPlugin;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_ExcaliburDrive extends BaseHullMod {

    public static final float EMP_REDUCTION = 50f;
    public static final float ENGINE_HEALTH_BONUS = 75f;
    public static final float FIGHTER_REFIT_REDUCTION = 33f;
    public static final float PROFILE_PENALTY = 100f;
    public static final float REPAIR_MAX_CAPITAL = 10f;
    public static final float REPAIR_MAX_CRUISER = 15f;
    public static final float REPAIR_MAX_DESTROYER = 20f;
    public static final float REPAIR_MAX_FRIGATE = 25f;
    public static final float REPAIR_RATE_CAPITAL = 0.25f;
    public static final float REPAIR_RATE_CRUISER = 0.5f;
    public static final float REPAIR_RATE_DESTROYER = 1f;
    public static final float REPAIR_RATE_FRIGATE = 2f;
    public static final float SENSOR_BONUS = 100f;
    public static final float SIGHT_BONUS = 100f;
    public static final float VENT_BONUS_CAPITAL = 20f;
    public static final float VENT_BONUS_CRUISER = 30f;
    public static final float VENT_BONUS_DESTROYER = 40f;
    public static final float VENT_BONUS_FRIGATE = 50f;
    public static final float WEAPON_FLUX_REDUCTION = 50f;

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);

    private static final float BURST_DAMAGE_FACTOR = 0.03f;
    private static final float BURST_DAMAGE_POWER = 1.4f;
    private static final float BURST_DO_NOT_USE_FLUX_DECISION_FACTOR = 0.5f;
    private static final float BURST_TARGET_FLUX_REMAINING_BONUS = 1f;
    private static final float HIGH_FLUX_THRESHOLD = 0.99f;

    static {
        BLOCKED_HULLMODS.add("efficiency_overhaul");
    }

    public static float getDModScale(ShipAPI ship) {
        if (ship == null) {
            return 1f;
        }
        int dmods = DModManager.getNumDMods(ship.getVariant());
        if (dmods > 3) {
            return 0f;
        } else if (dmods > 0) {
            return 0.9f - dmods * 0.2f;
        } else {
            return 1f;
        }
    }

    public static boolean shipIsInBurst(ShipAPI ship) {
        List<WeaponAPI> weapons = ship.getAllWeapons();
        int weaponsSize = weapons.size();
        for (int i = 0; i < weaponsSize; i++) {
            WeaponAPI weapon = weapons.get(i);
            if (weapon.getType() == WeaponType.DECORATIVE || weapon.getType() == WeaponType.LAUNCH_BAY
                    || weapon.getType() == WeaponType.SYSTEM) {
                continue;
            }
            if (((!weapon.isBeam() && weapon.getDerivedStats().getBurstFireDuration() > 0f
                    && weapon.getDerivedStats().getBurstFireDuration() <= 15f)
                    || weapon.isBurstBeam()) && (weapon.getChargeLevel() >= 0.75f || weapon.isFiring())
                    && weapon.getCooldownRemaining() <= 0.25f) {
                if (weapon.getSize() == WeaponSize.SMALL && (ship.getHullSize() == HullSize.FRIGATE
                        || ship.getHullSize() == HullSize.DESTROYER)) {
                    return true;
                }
                if (weapon.getSize() == WeaponSize.MEDIUM && (ship.getHullSize() == HullSize.FRIGATE
                        || ship.getHullSize() == HullSize.DESTROYER
                        || ship.getHullSize() == HullSize.CRUISER)) {
                    return true;
                }
                if (weapon.getSize() == WeaponSize.LARGE) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void markMemberExploded(FleetMemberAPI member) {
        if ((member != null) && (Global.getSector() != null) && (Global.getSector().getPersistentData() != null)) {
            Set<FleetMemberAPI> explodedMembers;
            if (Global.getSector().getPersistentData().containsKey("tem_excaliburcore_explosion")) {
                explodedMembers = (Set<FleetMemberAPI>) Global.getSector().getPersistentData().get("tem_excaliburcore_explosion");
            } else {
                explodedMembers = new HashSet<>(1000);
            }

            member.getStats().getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyFlat("tem_excaliburcore_explosion", -10000f);
            explodedMembers.add(member);
        }
    }

    @Override
    public void advanceInCampaign(FleetMemberAPI member, float amount) {
        if ((member != null) && (Global.getSector() != null) && (Global.getSector().getPersistentData() != null)) {
            Set<FleetMemberAPI> explodedMembers;
            if (Global.getSector().getPersistentData().containsKey("tem_excaliburcore_explosion")) {
                explodedMembers = (Set<FleetMemberAPI>) Global.getSector().getPersistentData().get("tem_excaliburcore_explosion");
            } else {
                explodedMembers = new HashSet<>(1000);
            }

            if (explodedMembers.contains(member)) {
                member.getStats().getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyFlat("tem_excaliburcore_explosion", -10000f);
            }
        }
    }

    private static void almaceBurstAI(ShipAPI ship) {
        boolean shouldUseSystem = false;

        List<DamagingProjectileAPI> projectiles = Global.getCombatEngine().getProjectiles();
        List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(projectiles.size() / 10);
        for (DamagingProjectileAPI tmp : projectiles) {
            if (MathUtils.isWithinRange(tmp.getLocation(), ship.getLocation(), ship.getCollisionRadius() * 5f)) {
                nearbyThreats.add(tmp);
            }
        }
        nearbyThreats = CollectionUtils.filter(nearbyThreats, new FilterMissesAlmace(ship));
        float area = TEM_AlmaceBurstPlugin.getExpectedArea(ship);

        float decisionLevel = 0f;
        for (DamagingProjectileAPI threat : nearbyThreats) {
            if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                decisionLevel += Math.pow(threat.getDamageAmount() * (float) Math.sqrt(TEM_LatticeShield.shieldLevel(
                        ship)) * 0.5f * BURST_DAMAGE_FACTOR, BURST_DAMAGE_POWER);
            } else {
                decisionLevel += Math.pow(threat.getDamageAmount() * (float) Math.sqrt(TEM_LatticeShield.shieldLevel(
                        ship)) * BURST_DAMAGE_FACTOR, BURST_DAMAGE_POWER);
            }
        }

        float targetedEnemies = 0f;
        List<ShipAPI> targets = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
        Iterator<ShipAPI> iter = targets.iterator();
        while (iter.hasNext()) {
            ShipAPI target = iter.next();
            if (!target.isAlive() || target.getOwner() == ship.getOwner() || target.getCollisionClass()
                    == CollisionClass.NONE) {
                continue;
            }

            float value = 0f;

            FleetMemberAPI member = CombatUtils.getFleetMember(target);
            float targetStrength = 1f;
            if (member != null) {
                targetStrength = 0.1f + member.getFleetPointCost();
            }
            if (target.isFighter() || target.isDrone()) {
                value = 0.25f;
            } else if (target.isFrigate()) {
                value = targetStrength;
            } else if (target.isDestroyer()) {
                value = targetStrength;
            } else if (target.isCruiser()) {
                value = targetStrength;
            } else if (target.isCapital()) {
                value = targetStrength;
            }

            if (target.getShield() != null && target.getShield().isOn()) {
                if (target.getShield().isWithinArc(ship.getLocation())) {
                    value *= 0.5f;
                } else {
                    value *= 0.75f;
                }
            }

            value *= 1f - MathUtils.getDistance(ship, target) / area;

            targetedEnemies += value;
        }

        decisionLevel += targetedEnemies;
        decisionLevel *= 2f - ship.getHullLevel();
        ShipwideAIFlags flags = ship.getAIFlags();
        if (flags != null && flags.hasFlag(ShipwideAIFlags.AIFlags.DO_NOT_USE_FLUX)) {
            decisionLevel *= BURST_DO_NOT_USE_FLUX_DECISION_FACTOR;
        }
        if (flags != null && !flags.hasFlag(ShipwideAIFlags.AIFlags.HAS_INCOMING_DAMAGE)) {
            decisionLevel *= 0.75f;
        }
        if (shipIsInBurst(ship)) {
            decisionLevel *= 0.25f;
        }

        if (decisionLevel >= 0.5f * Math.sqrt(ship.getMaxHitpoints() / 20f)) {
            shouldUseSystem = true;
        }

        if (shouldUseSystem && ((ship.getFluxTracker().getCurrFlux() + TEM_AlmaceBurstPlugin.FLUX_COST
                * ship.getHullSpec().getFluxCapacity())
                < (ship.getFluxTracker().getMaxFlux() * HIGH_FLUX_THRESHOLD))) {
            TEM_AlmaceBurstPlugin.activate(ship);
        }
    }

    private static void prewenBurstShieldAI(ShipAPI ship) {
        boolean shouldUseSystem = false;

        List<DamagingProjectileAPI> projectiles = Global.getCombatEngine().getProjectiles();
        List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(projectiles.size() / 10);
        for (DamagingProjectileAPI tmp : projectiles) {
            if (MathUtils.isWithinRange(tmp.getLocation(), ship.getLocation(), ship.getCollisionRadius() * 5f)) {
                nearbyThreats.add(tmp);
            }
        }
        nearbyThreats = CollectionUtils.filter(nearbyThreats, new FilterMissesPriwen(ship));
        float area = TEM_PriwenBurstPlugin.getExpectedArea(ship);
        List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, area);
        for (MissileAPI missile : nearbyMissiles) {
            if (TEM_PriwenBurstPlugin.getExpectedDamage(ship, MathUtils.getDistance(ship, missile))
                    < missile.getHitpoints()) {
                continue;
            }

            nearbyThreats.add(missile);
        }

        float decisionLevel = 0f;
        for (DamagingProjectileAPI threat : nearbyThreats) {
            if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                decisionLevel += Math.pow(threat.getDamageAmount() * (float) Math.sqrt(TEM_LatticeShield.shieldLevel(
                        ship)) * 0.5f * BURST_DAMAGE_FACTOR, BURST_DAMAGE_POWER);
            } else {
                decisionLevel += Math.pow(threat.getDamageAmount() * (float) Math.sqrt(TEM_LatticeShield.shieldLevel(
                        ship)) * BURST_DAMAGE_FACTOR, BURST_DAMAGE_POWER);
            }
        }

        float targetedEnemies = 0f;
        List<ShipAPI> targets = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
        Iterator<ShipAPI> iter = targets.iterator();
        while (iter.hasNext()) {
            ShipAPI target = iter.next();
            if (!target.isAlive() || target.getOwner() == ship.getOwner() || target.getCollisionClass()
                    == CollisionClass.NONE) {
                continue;
            }

            float value = 0f;
            float fluxRemaining = target.getFluxTracker().getMaxFlux() - target.getFluxTracker().getCurrFlux();
            float threshold = TEM_PriwenBurstPlugin.getExpectedFlux(target, MathUtils.getDistance(ship, target));
            float bonus = BURST_TARGET_FLUX_REMAINING_BONUS * Math.max(0f, (threshold - fluxRemaining) / threshold) + 1f;

            if (target.getFluxTracker().isOverloaded() || target.getFluxTracker().isVenting()) {
                bonus *= 0f;
            }

            FleetMemberAPI member = CombatUtils.getFleetMember(target);
            float targetStrength = 1f;
            if (member != null) {
                targetStrength = 0.1f + member.getFleetPointCost();
            }
            if (target.isFighter() || target.isDrone()) {
                value = 0.25f + 0.25f * bonus;
            } else if (target.isFrigate()) {
                value = targetStrength + 1.5f * bonus;
            } else if (target.isDestroyer()) {
                value = targetStrength + 2f * bonus;
            } else if (target.isCruiser()) {
                value = targetStrength + 2.5f * bonus;
            } else if (target.isCapital()) {
                value = targetStrength + 3f * bonus;
            }

            if (target.getShield() != null && target.getShield().isOn()) {
                if (target.getShield().isWithinArc(ship.getLocation())) {
                    value *= 0.5f;
                } else {
                    value *= 0.75f;
                }
            }

            value *= 1f - MathUtils.getDistance(ship, target) / area;

            targetedEnemies += value;
        }

        decisionLevel += targetedEnemies;
        decisionLevel *= 2f - ship.getHullLevel();
        ShipwideAIFlags flags = ship.getAIFlags();
        if (flags != null && flags.hasFlag(ShipwideAIFlags.AIFlags.DO_NOT_USE_FLUX)) {
            decisionLevel *= BURST_DO_NOT_USE_FLUX_DECISION_FACTOR;
        }
        if (flags != null && !flags.hasFlag(ShipwideAIFlags.AIFlags.HAS_INCOMING_DAMAGE)) {
            decisionLevel *= 0.75f;
        }
        if (shipIsInBurst(ship)) {
            decisionLevel *= 0.25f;
        }

        if (decisionLevel >= 0.5f * Math.sqrt(ship.getMaxHitpoints() / 20f)) {
            shouldUseSystem = true;
        }

        if (shouldUseSystem && ((ship.getFluxTracker().getCurrFlux() + TEM_PriwenBurstPlugin.FLUX_COST
                * ship.getHullSpec().getFluxCapacity())
                < (ship.getFluxTracker().getMaxFlux() * HIGH_FLUX_THRESHOLD))) {
            TEM_PriwenBurstPlugin.activate(ship);
        }
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        if (!Global.getCombatEngine().isEntityInPlay(ship) || Global.getCombatEngine().isPaused() || !ship.isAlive()) {
            return;
        }

        if (ship.getFluxTracker().getCurrFlux() <= 1f) {
            ship.getFluxTracker().setCurrFlux(1f);
        }

        if (ship.getShipAI() == null) {
            return;
        }

        float scale = getDModScale(ship);
        if (scale <= 0f) {
            return;
        }

        switch (ship.getHullSpec().getBaseHullId()) {
            case "tem_jesuit":
            case "tem_crusader":
            case "tem_paladin":
            case "tem_chevalier":
            case "tem_archbishop":
            case "tem_boss_paladin":
            case "tem_boss_archbishop":
                prewenBurstShieldAI(ship);
                break;
            case "tem_martyr":
            case "tem_teuton":
                almaceBurstAI(ship);
                break;
            default:
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
                TEM_BlockedHullmodDisplayScript.showBlocked(ship);
            }
        }

        float equipFraction = getTemplarEquipFraction(ship);
        float reductionFraction = 1f;
        if (equipFraction < 1f) {
            reductionFraction = Math.max(0f, equipFraction - 0.5f);
        }

        ship.getMutableStats().getBallisticWeaponFluxCostMod().modifyMult(id, 1f - (WEAPON_FLUX_REDUCTION * reductionFraction / 100f));
        ship.getMutableStats().getEnergyWeaponFluxCostMod().modifyMult(id, 1f - (WEAPON_FLUX_REDUCTION * reductionFraction / 100f));
        ship.getMutableStats().getMissileWeaponFluxCostMod().modifyMult(id, 1f - (WEAPON_FLUX_REDUCTION * reductionFraction / 100f));
        ship.getMutableStats().getFighterRefitTimeMult().modifyMult(id, 1f - (FIGHTER_REFIT_REDUCTION * reductionFraction / 100f));

        float modifiedHull = ship.getMutableStats().getHullBonus().computeEffective(ship.getHullSpec().getHitpoints());
        float unmodifiedHull = ship.getHullSpec().getHitpoints();
        float repairFraction = unmodifiedHull / Math.max(1f, modifiedHull);

        switch (ship.getHullSize()) {
            case FRIGATE:
            case FIGHTER:
                ship.getMutableStats().getHullCombatRepairRatePercentPerSecond().modifyFlat(id, REPAIR_RATE_FRIGATE * repairFraction);
                ship.getMutableStats().getMaxCombatHullRepairFraction().modifyFlat(id, REPAIR_MAX_FRIGATE * repairFraction / 100f);
                break;
            case DESTROYER:
                ship.getMutableStats().getHullCombatRepairRatePercentPerSecond().modifyFlat(id, REPAIR_RATE_DESTROYER * repairFraction);
                ship.getMutableStats().getMaxCombatHullRepairFraction().modifyFlat(id, REPAIR_MAX_DESTROYER * repairFraction / 100f);
                break;
            case CRUISER:
                ship.getMutableStats().getHullCombatRepairRatePercentPerSecond().modifyFlat(id, REPAIR_RATE_CRUISER * repairFraction);
                ship.getMutableStats().getMaxCombatHullRepairFraction().modifyFlat(id, REPAIR_MAX_CRUISER * repairFraction / 100f);
                break;
            default:
            case CAPITAL_SHIP:
                ship.getMutableStats().getHullCombatRepairRatePercentPerSecond().modifyFlat(id, REPAIR_RATE_CAPITAL * repairFraction);
                ship.getMutableStats().getMaxCombatHullRepairFraction().modifyFlat(id, REPAIR_MAX_CAPITAL * repairFraction / 100f);
                break;
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getEmpDamageTakenMult().modifyPercent(id, -EMP_REDUCTION);
        stats.getEngineHealthBonus().modifyPercent(id, ENGINE_HEALTH_BONUS);
        stats.getSightRadiusMod().modifyPercent(id, SIGHT_BONUS);
        if (null != hullSize) {
            switch (hullSize) {
                case FRIGATE:
                case FIGHTER:
                    stats.getVentRateMult().modifyPercent(id, VENT_BONUS_FRIGATE);
                    break;
                case DESTROYER:
                    stats.getVentRateMult().modifyPercent(id, VENT_BONUS_DESTROYER);
                    break;
                case CRUISER:
                    stats.getVentRateMult().modifyPercent(id, VENT_BONUS_CRUISER);
                    break;
                default:
                case CAPITAL_SHIP:
                    stats.getVentRateMult().modifyPercent(id, VENT_BONUS_CAPITAL);
                    break;
            }
        }
        stats.getSensorStrength().modifyPercent(id, SENSOR_BONUS);
        stats.getSensorProfile().modifyPercent(id, PROFILE_PENALTY);
        stats.getZeroFluxSpeedBoost().modifyMult(id, 0.01f);
        stats.getZeroFluxMinimumFluxLevel().modifyMult(id, 0f);
        stats.getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyMult(id, 0.5f);
        if (stats.getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).getFlatBonus() > 0f) {
            stats.getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyFlat(id, -1f * stats.getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).getFlatBonus());
        }
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize, ShipAPI ship) {
        float equipFraction = getTemplarEquipFraction(ship);
        float reductionFraction = 1f;
        if (equipFraction < 1f) {
            reductionFraction = Math.max(0f, equipFraction - 0.5f);
        }
        float scale = getDModScale(ship);
        switch (index) {
            case 0:
                switch (hullSize) {
                    case FRIGATE:
                    case FIGHTER:
                        return "" + (int) VENT_BONUS_FRIGATE + "%";
                    case DESTROYER:
                        return "" + (int) VENT_BONUS_DESTROYER + "%";
                    case CRUISER:
                        return "" + (int) VENT_BONUS_CRUISER + "%";
                    case CAPITAL_SHIP:
                    default:
                        return "" + (int) VENT_BONUS_CAPITAL + "%";
                }
            case 1:
                return "halved";
            case 2:
                return "" + (int) ENGINE_HEALTH_BONUS + "%";
            case 3:
                return "" + Math.round(WEAPON_FLUX_REDUCTION * reductionFraction) + "%";
            case 4:
                return "" + Math.round(FIGHTER_REFIT_REDUCTION * reductionFraction) + "%";
            case 5:
                if (equipFraction < 1f) {
                    return " " + Math.round((1f - equipFraction) * 100f) + "% non-Templar equipment is installed!";
                } else {
                    return "";
                }
            case 6:
                return "twice";
            case 7:
                return "doubles";
            case 8: {
                if (scale >= 1f) {
                    return "";
                } else {
                    return "\n\n";
                }
            }
            case 9: {
                if (scale >= 1f) {
                    return "";
                } else {
                    switch (ship.getHullSpec().getBaseHullId()) {
                        case "tem_jesuit":
                        case "tem_crusader":
                        case "tem_paladin":
                        case "tem_chevalier":
                        case "tem_archbishop":
                        case "tem_boss_paladin":
                        case "tem_boss_archbishop":
                            return "Lasting damage reduces Priwen Burst capability by " + Math.round((1f - scale) * 100f) + "%!";
                        case "tem_martyr":
                        case "tem_teuton":
                            return "Lasting damage reduces Almace Burst capability by " + Math.round((1f - scale) * 100f) + "%!";
                        default:
                            return "";
                    }
                }
            }
            default:
                return null;
        }
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && ship.getHullSpec().getHullId().startsWith("tem_");
    }

    private static float getTemplarEquipFraction(ShipAPI ship) {
        if (ship == null) {
            return 1f;
        }

        float totalEquipOP = 0;
        float templarEquipOP = 0;
        for (WeaponAPI weapon : ship.getAllWeapons()) {
            totalEquipOP += Math.max(1, weapon.getSpec().getOrdnancePointCost(null));
            if (weapon.getId().startsWith("tem_")) {
                templarEquipOP += Math.max(1, weapon.getSpec().getOrdnancePointCost(null));
            }
        }
        for (FighterWingAPI wing : ship.getAllWings()) {
            totalEquipOP += Math.max(1, wing.getSpec().getOpCost(null));
            if (wing.getWingId().startsWith("tem_")) {
                templarEquipOP += Math.max(1, wing.getSpec().getOpCost(null));
            }
        }

        if (totalEquipOP < 1) {
            return 1f;
        } else {
            return templarEquipOP / totalEquipOP;
        }
    }

    private static final class FilterMissesAlmace implements CollectionUtils.CollectionFilter<DamagingProjectileAPI> {

        private final ShipAPI ship;

        private FilterMissesAlmace(ShipAPI ship) {
            this.ship = ship;
        }

        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner()) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare() || missile.isFizzling() || missile.getMissileAI() == null) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(),
                    Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(Math.max(TEM_AlmaceBurstPlugin.getExpectedInvulnerabilityTime(ship) - 0.25f, 0f)), null),
                    ship.getLocation(), ship.getCollisionRadius())
                    && (Math.abs(MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f));
        }
    };

    private static final class FilterMissesPriwen implements CollectionUtils.CollectionFilter<DamagingProjectileAPI> {

        private final ShipAPI ship;

        private FilterMissesPriwen(ShipAPI ship) {
            this.ship = ship;
        }

        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner()) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare() || missile.isFizzling() || missile.getMissileAI() == null) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(),
                    Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(Math.max(TEM_PriwenBurstPlugin.getExpectedInvulnerabilityTime(ship) - 0.25f, 0f)), null),
                    ship.getLocation(), ship.getCollisionRadius())
                    && (Math.abs(MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f));
        }
    };
}
