package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CampaignTerrainAPI;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.procgen.DropGroupRow;
import com.fs.starfarer.api.impl.campaign.procgen.SalvageEntityGenDataSpec.DropData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipRecoverySpecialData;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin.DebrisFieldSource;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TEM_FleetCheating implements EveryFrameScript {

    private final IntervalUtil interval = new IntervalUtil(1f, 2f);

    @Override
    @SuppressWarnings("unchecked")
    public void advance(float amount) {
        interval.advance(amount);

        if (interval.intervalElapsed()) {
            ArrayList<DropGroupRow> toRemove = new ArrayList<>(10);
            ArrayList<SectorEntityToken> entitiesToRemove = new ArrayList<>(10);

            List<CampaignFleetAPI> fleets = new ArrayList<>(500);
            fleets.addAll(Global.getSector().getHyperspace().getFleets());
            List<StarSystemAPI> systems = Global.getSector().getStarSystems();
            for (StarSystemAPI system : systems) {
                fleets.addAll(system.getFleets());

                CampaignFleetAPI player = Global.getSector().getPlayerFleet();
                if (player != null && player.getContainingLocation() == system) {
                    /* Remove templar shit from debris fields and derelicts - no free templar stuff for you! */
                    for (CampaignTerrainAPI curr : system.getTerrainCopy()) {
                        if (curr.getPlugin() instanceof DebrisFieldTerrainPlugin) {
                            DebrisFieldTerrainPlugin plugin = (DebrisFieldTerrainPlugin) curr.getPlugin();
                            if (plugin.params.source == DebrisFieldSource.BATTLE) {
                                for (DropData drop : curr.getDropRandom()) {
                                    WeightedRandomPicker<DropGroupRow> custom = drop.getCustom();
                                    if (custom != null) {
                                        for (DropGroupRow row : custom.getItems()) {
                                            if (row.isWeapon() && (row.getWeaponId() != null) && row.getWeaponId().startsWith("tem_")) {
                                                toRemove.add(row);
                                            } else if (row.isFighterWing() && (row.getFighterWingId() != null) && row.getFighterWingId().startsWith("tem_")) {
                                                toRemove.add(row);
                                            } else if (row.isSpecialItem() && (row.getSpecialItemId() != null) && row.getSpecialItemId().startsWith("tem_")) {
                                                toRemove.add(row);
                                            }
                                        }

                                        for (DropGroupRow row : toRemove) {
                                            custom.remove(row);
                                        }
                                        toRemove.clear();
                                    }
                                }

                                ShipRecoverySpecialData data = ShipRecoverySpecial.getSpecialData(curr, null, false, false);
                                if (data != null) {
                                    Iterator<PerShipData> iter = data.ships.iterator();
                                    while (iter.hasNext()) {
                                        PerShipData shipData = iter.next();
                                        if ((shipData.variant != null) && shipData.variant.getHullSpec().getHullId().startsWith("tem_")) {
                                            iter.remove();
                                            continue;
                                        }
                                        if ((shipData.variantId != null) && shipData.variantId.startsWith("tem_")) {
                                            iter.remove();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (Iterator<CustomCampaignEntityAPI> it = system.getEntities(CustomCampaignEntityAPI.class).iterator(); it.hasNext();) {
                        CustomCampaignEntityAPI wreck = it.next();
                        ShipRecoverySpecialData data = ShipRecoverySpecial.getSpecialData(wreck, null, false, false);
                        if (data != null) {
                            Iterator<PerShipData> iter = data.ships.iterator();
                            while (iter.hasNext()) {
                                PerShipData shipData = iter.next();
                                if ((shipData.variant != null) && shipData.variant.getHullSpec().getHullId().startsWith("tem_")) {
                                    iter.remove();
                                    continue;
                                }
                                if ((shipData.variantId != null) && shipData.variantId.startsWith("tem_")) {
                                    iter.remove();
                                }
                            }

                            if (data.ships.isEmpty()) {
                                entitiesToRemove.add(wreck);
                            }
                        }
                    }

                    for (SectorEntityToken entity : entitiesToRemove) {
                        system.removeEntity(entity);
                    }
                    entitiesToRemove.clear();
                }
            }

            for (CampaignFleetAPI fleet : fleets) {
                if (fleet.getFaction().getId().contentEquals("templars")) {
                    List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
                    for (FleetMemberAPI member : members) {
                        if (member.isFighterWing()) {
                            member.getRepairTracker().setCR(Math.min(member.getRepairTracker().getBaseCR() + 0.04f, member.getRepairTracker().getMaxCR()));
                        } else if (!member.getRepairTracker().isSuspendRepairs() && !member.getRepairTracker().isMothballed()) {
                            float fraction = member.getRepairTracker().computeRepairednessFraction();
                            if (fraction <= 0.5f) {
                                member.getRepairTracker().performRepairsFraction(0.5f - fraction);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
}
