package data.scripts.campaign;

import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.BaseCampaignPlugin;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetInflater;
import com.fs.starfarer.api.campaign.InteractionDialogPlugin;
import com.fs.starfarer.api.campaign.ReputationActionResponsePlugin;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActionEnvelope;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActions;
import com.fs.starfarer.api.impl.campaign.RuleBasedInteractionDialogPluginImpl;
import com.fs.starfarer.api.impl.campaign.fleets.DefaultFleetInflater;
import com.fs.starfarer.api.impl.campaign.fleets.DefaultFleetInflaterParams;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import data.scripts.world.templars.TEM_Antioch;

public class TEM_CampaignPluginImpl extends BaseCampaignPlugin {

    @Override
    public String getId() {
        return "TEM_CampaignPluginImpl";
    }

    @Override
    public boolean isTransient() {
        return true;
    }

    @Override
    public PluginPick<InteractionDialogPlugin> pickInteractionDialogPlugin(SectorEntityToken interactionTarget) {
        if (interactionTarget == TEM_Antioch.getAscalon()) {
            interactionTarget.removeTag(Tags.COMM_RELAY);
            return new PluginPick<InteractionDialogPlugin>(new RuleBasedInteractionDialogPluginImpl(), PickPriority.MOD_SPECIFIC);
        }
        return null;
    }

    @Override
    public PluginPick<ReputationActionResponsePlugin> pickReputationActionResponsePlugin(Object action, String factionId) {
        if (action instanceof RepActionEnvelope) {
            if (factionId.contentEquals("templars")) {
                RepActionEnvelope envelope = (RepActionEnvelope) action;
                if (envelope.action == RepActions.REP_DECAY_POSITIVE) { // Hahaha, no free rep for you, assholes
                    return new PluginPick<ReputationActionResponsePlugin>(new TEM_ReputationPlugin(), PickPriority.MOD_SET);
                }
            }
        }
        if (action instanceof RepActions) {
            if (factionId.contentEquals("templars")) {
                RepActions actions = (RepActions) action;
                if (actions == RepActions.REP_DECAY_POSITIVE) { // Hahaha, no free rep for you, assholes
                    return new PluginPick<ReputationActionResponsePlugin>(new TEM_ReputationPlugin(), PickPriority.MOD_SET);
                }
            }
        }

        return null;
    }

    @Override
    public PluginPick<FleetInflater> pickFleetInflater(CampaignFleetAPI fleet, Object params) {
        if (fleet.getFaction().getId().contentEquals("templars")) {
            if (params instanceof DefaultFleetInflaterParams) {
                DefaultFleetInflaterParams p = (DefaultFleetInflaterParams) params;
                p.allWeapons = true;
                p.quality = 3f;
                return new PluginPick<FleetInflater>(new DefaultFleetInflater(p), PickPriority.MOD_SET);
            }
        }
        return null;
    }
}
