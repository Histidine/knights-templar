package data.scripts.everyframe;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.input.InputEventType;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.TEM_AnamorphicFlare;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import static data.scripts.everyframe.TEM_PriwenBurstPlugin.PRIWEN_BUTTON;
import static data.scripts.everyframe.TEM_PriwenBurstPlugin.PRIWEN_KEY;
import static data.scripts.everyframe.TEM_PriwenBurstPlugin.PRIWEN_MOUSE;
import data.scripts.hullmods.TEM_ExcaliburDrive;

public class TEM_AlmaceBurstPlugin extends BaseEveryFrameCombatPlugin {

    public static final float FLUX_COST = 0.15f;

    private static final Color COLOR1 = new Color(150, 200, 255);
    private static final Color COLOR2 = new Color(50, 200, 255);
    private static final Color COLOR3 = new Color(65, 205, 255);
    private static final Color COLOR4 = new Color(50, 255, 255, 100);
    private static final Color COLOR5 = new Color(100, 200, 255);
    private static final Color COLOR6 = new Color(255, 255, 255);
    private static final Color COLOR7 = new Color(150, 255, 255);
    private static final Color COLOR_JITTER = new Color(50, 200, 255, 50);

    private static final String DATA_KEY = "TEM_AlmaceBurst";

    private static final Map<HullSize, Float> PITCH_BEND = new HashMap<>(4);

    private static final Vector2f ZERO = new Vector2f();

    static {
        PITCH_BEND.put(HullSize.FRIGATE, 1f);
        PITCH_BEND.put(HullSize.DESTROYER, 0.9f);
        PITCH_BEND.put(HullSize.CRUISER, 0.825f);
        PITCH_BEND.put(HullSize.CAPITAL_SHIP, 0.775f);
    }

    public static void activate(ShipAPI ship) {
        float scale = TEM_ExcaliburDrive.getDModScale(ship);
        if (scale <= 0f) {
            return;
        }

        if (TEM_AlmaceBurstPlugin.cooldownTime(ship) <= 0f && (ship.getCurrentCR() > 0f || ship.isFighter())) {
            TEM_AlmaceBurstPlugin.addBursting(ship);
        }
    }

    public static void addBursting(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return;
        }

        final Map<ShipAPI, AlmaceBurstData> bursting = localData.bursting;

        bursting.put(ship, new AlmaceBurstData());
    }

    public static float chargeLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> chargingLevel = localData.chargingLevel;

        if (chargingLevel.containsKey(ship)) {
            return chargingLevel.get(ship);
        } else {
            return 0f;
        }
    }

    public static float cooldownTime(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        if (coolingDown.containsKey(ship)) {
            return Global.getCombatEngine().getTotalElapsedTime(false) - coolingDown.get(ship);
        } else {
            return 0f;
        }
    }

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, AlmaceBurstData> bursting = localData.bursting;

        if (bursting.containsKey(ship)) {
            return bursting.get(ship).currentLevel;
        } else {
            return 0f;
        }
    }

    public static float getExpectedArea(ShipAPI ship) {
        return (float) Math.sqrt(ship.getCollisionRadius() / 5f) * 150f;
    }

    public static float getExpectedDamage(ShipAPI ship, float distance) {
        float falloff = Math.max(0f, 1f - distance / getExpectedArea(ship));
        return falloff * 10f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux() / 2f);
    }

    public static float getExpectedInvulnerabilityTime(ShipAPI ship) {
        return 0.5f;
    }

    public static void removeFromCooldown(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;
        final Map<ShipAPI, Float> trueCoolingDown = localData.trueCoolingDown;

        if (coolingDown.containsKey(ship)) {
            coolingDown.remove(ship);
        }
        if (trueCoolingDown.containsKey(ship)) {
            trueCoolingDown.remove(ship);
        }
    }

    public static float trueCooldownTime(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return -1f;
        }

        final Map<ShipAPI, Float> trueCoolingDown = localData.trueCoolingDown;

        if (trueCoolingDown.containsKey(ship)) {
            return Global.getCombatEngine().getTotalElapsedTime(false) - trueCoolingDown.get(ship);
        } else {
            return -1f;
        }
    }

    private CombatEngineAPI engine;
    private final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null) {
            return;
        }

        processInput(events);

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, AlmaceBurstData> bursting = localData.bursting;
        final Map<ShipAPI, Float> chargingLevel = localData.chargingLevel;
        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;
        final Map<ShipAPI, Float> trueCoolingDown = localData.trueCoolingDown;
        final Map<ShipAPI, Object> uiKey = localData.uiKey;

        interval.advance(amount);
        boolean intervalElapsed = interval.intervalElapsed();

        Iterator<Map.Entry<ShipAPI, AlmaceBurstData>> iter = bursting.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<ShipAPI, AlmaceBurstData> entry = iter.next();
            ShipAPI ship = entry.getKey();
            AlmaceBurstData data = entry.getValue();

            float scale = TEM_ExcaliburDrive.getDModScale(ship);
            if (scale <= 0f) {
                continue;
            }

            float shipRadius = TEM_Util.effectiveRadius(ship);

            if (ship == engine.getPlayerShip()) {
                if (!uiKey.containsKey(ship)) {
                    uiKey.put(ship, new Object());
                }
                engine.maintainStatusForPlayerShip(uiKey.get(ship), "graphics/icons/hullsys/emp_emitter.png",
                        "Almace Burst", "Smiting nearby hostile ship", false);
            }

            if (engine.isPaused()) {
                continue;
            }

            if (data.chargeLevel < 1f && !data.done) {
                if (!data.started) {
                    ship.getFluxTracker().increaseFlux(
                            ship.getMutableStats().getPhaseCloakActivationCostBonus().computeEffective(
                                    ship.getMutableStats().getFluxCapacity().getBaseValue() * FLUX_COST), true);
                    if (ship.getFluxTracker().isOverloadedOrVenting()) {
                        iter.remove();
                        continue;
                    }

                    data.started = true;
                    StandardLight light = new StandardLight(ZERO, ZERO, ZERO, ship);
                    light.setColor(COLOR1);
                    light.setSize(shipRadius * 2f * scale);
                    light.setIntensity(3f * scale);
                    light.fadeIn(0.5f);
                    light.setLifetime(0f);
                    LightShader.addLight(light);
                    float time = 0.5f;
                    Global.getSoundPlayer().playSound("tem_almaceburst_activate", 0.4f / time, 1f * scale, ship.getLocation(),
                            ship.getVelocity());
                    coolingDown.put(ship, engine.getTotalElapsedTime(false));
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float chargingTime = 0.5f;
                data.chargeLevel = Math.min(data.chargeLevel + amount
                        * ship.getMutableStats().getTimeMult().getModifiedValue() / chargingTime, 1f);
                chargingLevel.put(ship, data.chargeLevel);

                if (intervalElapsed) {
                    float radius = 6f * shipRadius * ((float) Math.random() * 0.5f + 0.5f) * scale;
                    Vector2f direction = MathUtils.getRandomPointOnCircumference(null, shipRadius * 2.5f * scale);
                    Vector2f point = MathUtils.getPointOnCircumference(ship.getLocation(), radius,
                            VectorUtils.getFacing(direction));
                    direction.scale(-1.5f);
                    Vector2f.add(ship.getVelocity(), direction, direction);
                    engine.addSmoothParticle(point, direction, 7.5f, 1f * scale, chargingTime, COLOR2);
                }

                float alphaMult;
                if (data.chargeLevel < 0.3f) {
                    alphaMult = 1f - data.chargeLevel * 2f;
                } else if (data.chargeLevel < 0.7f) {
                    alphaMult = 0.4f;
                } else {
                    alphaMult = 0.4f + (data.chargeLevel - 0.7f) * 2f;
                }
                float jitterLevel = 1.25f * scale / alphaMult;
                ship.setExtraAlphaMult(alphaMult);
                ship.setJitter(this, COLOR_JITTER, jitterLevel,
                        Math.max(2, Math.round(jitterLevel * 4f)),
                        (float) Math.sqrt(jitterLevel) * shipRadius * 0.02f,
                        jitterLevel * shipRadius * 0.04f);
            } else {
                float unchargingTime = 0.5f;
                float area = (float) Math.sqrt(ship.getCollisionRadius() / 5f) * 150f * scale;
                float damage = 10f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux() / 2f) * scale;
                float emp = 5f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux() / 2f) * scale;
                data.chargeLevel = Math.max(data.chargeLevel - amount
                        * ship.getMutableStats().getTimeMult().getModifiedValue() / unchargingTime, 0f);
                chargingLevel.put(ship, data.chargeLevel);

                if (!data.done) {
                    if (ship.isFighter()) {
                        ship.setCollisionClass(CollisionClass.FIGHTER);
                    } else {
                        ship.setCollisionClass(CollisionClass.SHIP);
                    }
                    trueCoolingDown.put(ship, engine.getTotalElapsedTime(false));
                    data.done = true;
                    engine.addHitParticle(ship.getLocation(), ZERO, area * 1.5f, 1f, unchargingTime, COLOR3);
                    TEM_AnamorphicFlare.createFlare(ship, new Vector2f(ship.getLocation()), engine, 0.5f * scale, 0.1f, 0f, 0f,
                            1f, COLOR2, COLOR3);

                    List<ShipAPI> nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
                    ShipAPI thisEnemy = null;
                    float distance, closestDistance = Float.MAX_VALUE;

                    for (ShipAPI tmp : nearbyEnemies) {
                        if (tmp.getCollisionClass() == CollisionClass.NONE || tmp.getOwner() == ship.getOwner()
                                || tmp.isHulk()) {
                            continue;
                        }
                        if (!TEM_Util.isWithinEmpRange(ship.getLocation(), area, tmp)) {
                            continue;
                        }
                        distance = MathUtils.getDistance(tmp, ship.getLocation());
                        if (distance < closestDistance) {
                            thisEnemy = tmp;
                            closestDistance = distance;
                        }
                    }

                    if (thisEnemy != null) {
                        engine.spawnExplosion(ship.getLocation(), ZERO, COLOR4, area, 0.1f);
                        float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
                        Global.getSoundPlayer().playSound("tem_almaceburst_zap", 1f, 1f * scale, ship.getLocation(), ZERO);
                        engine.spawnEmpArc(ship, ship.getLocation(), ship, thisEnemy, DamageType.KINETIC, damage
                                * falloff, emp * falloff, area * 1.25f, null,
                                (float) Math.sqrt(damage) * 1.5f, COLOR5, COLOR6);
                    }

                    StandardLight light = new StandardLight();
                    light.setLocation(ship.getLocation());
                    light.setColor(COLOR7);
                    light.setSize(area * 1.5f);
                    light.setIntensity(1f);
                    light.fadeOut(unchargingTime);
                    LightShader.addLight(light);

                    Global.getSoundPlayer().playSound("tem_almaceburst_blast", 1f, 1f, ship.getLocation(), ZERO);
                }
            }

            if (data.chargeLevel <= 0f) {
                if (ship.isFighter()) {
                    ship.setCollisionClass(CollisionClass.FIGHTER);
                } else {
                    ship.setCollisionClass(CollisionClass.SHIP);
                }
                chargingLevel.remove(ship);
                uiKey.remove(ship);
                iter.remove();
            } else {
                data.currentLevel = Math.min(data.chargeLevel, 1f);
            }
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    private void processInput(List<InputEventAPI> events) {
        if (engine.getCombatUI() == null) {
            return;
        }

        if (engine.getCombatUI().isShowingCommandUI()) {
            return;
        }

        ShipAPI player = engine.getPlayerShip();
        if (player == null || !engine.isEntityInPlay(player)) {
            return;
        }

        float scale = TEM_ExcaliburDrive.getDModScale(player);
        if (scale <= 0f) {
            return;
        }

        switch (TEM_Util.getNonDHullId(player.getHullSpec())) {
            case "tem_teuton":
            case "tem_martyr":
                break;
            default:
                return;
        }

        for (InputEventAPI event : events) {
            if (event.isConsumed()) {
                continue;
            }

            if (PRIWEN_MOUSE) {
                if (event.getEventType() == InputEventType.MOUSE_DOWN) {
                    if (event.getEventValue() == PRIWEN_BUTTON) {
                        event.consume();
                        activate(player);
                    }
                }
            } else {
                if (event.getEventType() == InputEventType.KEY_DOWN) {
                    if (event.getEventValue() == PRIWEN_KEY) {
                        event.consume();
                        activate(player);
                    }
                }
            }
        }
    }

    private static final class AlmaceBurstData {

        float chargeLevel = 0f;
        float currentLevel = 0f;
        boolean done = false;
        boolean started = false;
    }

    private static final class LocalData {

        final Map<ShipAPI, AlmaceBurstData> bursting = new LinkedHashMap<>(50);
        final Map<ShipAPI, Float> chargingLevel = new HashMap<>(50);
        final Map<ShipAPI, Float> coolingDown = new HashMap<>(50);
        final Map<ShipAPI, Float> trueCoolingDown = new HashMap<>(50);
        final Map<ShipAPI, Object> uiKey = new HashMap<>(50);
    }
}
