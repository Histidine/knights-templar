package data.scripts.everyframe;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import data.scripts.TEMModPlugin;

public class TEM_PluginStarter extends BaseEveryFrameCombatPlugin {

    @Override
    public void init(CombatEngineAPI engine) {
        if (TEMModPlugin.hasMagicLib) {
            engine.addPlugin(new TEM_Trails());
        }
    }
}
