package data.scripts.everyframe;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.SoundAPI;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.hullmods.TEM_ExcaliburDrive;
import data.scripts.util.TEM_AnamorphicFlare;
import data.scripts.util.TEM_Multi;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.dark.graphics.plugins.ShipDestructionEffects;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_ExcaliburDriveExplosion extends BaseEveryFrameCombatPlugin {

    private static final Color COLOR1 = new Color(150, 255, 255);
    private static final Color COLOR10 = new Color(255, 75, 50, 100);
    private static final Color COLOR11 = new Color(255, 100, 75, 150);
    private static final Color COLOR2 = new Color(50, 200, 255);
    private static final Color COLOR3 = new Color(50, 200, 255, 100);
    private static final Color COLOR4 = new Color(100, 200, 255, 150);
    private static final Color COLOR5 = new Color(65, 205, 255);
    private static final Color COLOR6 = new Color(200, 255, 255);
    private static final Color COLOR7 = new Color(65, 205, 255, 150);
    private static final Color COLOR8 = new Color(100, 255, 255);
    private static final Color COLOR9 = new Color(255, 255, 255);

    private static final String DATA_KEY = "TEM_ExacliburDriveExplosion";

    private static final Map<HullSize, Float> EXPLOSION_LENGTH = new HashMap<>(5);
    private static final Map<HullSize, Float> PITCH_BEND = new HashMap<>(5);
    private static final Map<HullSize, Integer> CONFETTI_PIECES = new HashMap<>(5);
    private static final Map<String, Float> EXPLOSION_CHANCE = new HashMap<>(5);

    private static final Vector2f ZERO = new Vector2f();

    static {
        PITCH_BEND.put(HullSize.FRIGATE, 1f);
        PITCH_BEND.put(HullSize.DESTROYER, 0.8f);
        PITCH_BEND.put(HullSize.CRUISER, 0.65f);
        PITCH_BEND.put(HullSize.CAPITAL_SHIP, 0.55f);
        PITCH_BEND.put(HullSize.DEFAULT, 0.55f);
    }

    static {
        EXPLOSION_LENGTH.put(HullSize.FRIGATE, 2.2f);
        EXPLOSION_LENGTH.put(HullSize.DESTROYER, 3.1f);
        EXPLOSION_LENGTH.put(HullSize.CRUISER, 4f);
        EXPLOSION_LENGTH.put(HullSize.CAPITAL_SHIP, 4.9f);
        EXPLOSION_LENGTH.put(HullSize.DEFAULT, 4.9f);
    }

    static {
        CONFETTI_PIECES.put(HullSize.FRIGATE, 5);
        CONFETTI_PIECES.put(HullSize.DESTROYER, 10);
        CONFETTI_PIECES.put(HullSize.CRUISER, 20);
        CONFETTI_PIECES.put(HullSize.CAPITAL_SHIP, 30);
        CONFETTI_PIECES.put(HullSize.DEFAULT, 30);
    }

    static {
        EXPLOSION_CHANCE.put("tem_martyr", 0.9f);
        EXPLOSION_CHANCE.put("tem_jesuit", 0.5f);
        EXPLOSION_CHANCE.put("tem_crusader", 0.6f);
        EXPLOSION_CHANCE.put("tem_paladin", 0.7f);
        EXPLOSION_CHANCE.put("tem_chevalier", 0.65f);
        EXPLOSION_CHANCE.put("tem_archbishop", 0.8f);
        EXPLOSION_CHANCE.put("tem_boss_paladin", 1f);
        EXPLOSION_CHANCE.put("tem_boss_archbishop", 1f);
    }

    private static void explode(CombatEngineAPI engine, ExplodingShip exploder) {
        ShipAPI ship = exploder.ship;
        int hulkOwner = ship.getOwner();
        ship.setOwner(ship.getOriginalOwner());

        float shipRadius = TEM_Util.effectiveRadius(ship);

        float unchargingTime = (float) Math.sqrt(Math.sqrt(exploder.powerLevel / 2f)) * EXPLOSION_LENGTH.get(
                ship.getHullSize());
        float area = (float) Math.sqrt(exploder.powerLevel) * (float) Math.sqrt(ship.getCollisionRadius()) * 90f;
        float damage = exploder.powerLevel * 3f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
        float maxDamage = damage * 7.5f;
        float emp = exploder.powerLevel * 8f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
        float flux = exploder.powerLevel * 12f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
        float force = exploder.powerLevel * 5f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());

        for (int i = 0; i <= exploder.powerLevel * (float) Math.sqrt(ship.getCollisionRadius()) * 2f / PITCH_BEND.get(ship.getHullSize()); i++) {
            float angle = (float) Math.random() * 360f;
            float distance = ((float) Math.random() * area * 0.3f + area * 0.3f) / PITCH_BEND.get(ship.getHullSize());
            Vector2f point1 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle);
            Vector2f point2 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle + 45f * (float) Math.random());
            engine.spawnEmpArc(ship, point1, new SimpleEntity(point1), new SimpleEntity(point2), DamageType.ENERGY, 0f,
                    0f, 1000f, null, exploder.powerLevel * (exploder.chargeLevel * 20f + 20f), COLOR5, COLOR6);
        }
        for (int i = 0; i <= exploder.powerLevel * ship.getCollisionRadius() * 0.3f / PITCH_BEND.get(ship.getHullSize()); i++) {
            if (Math.random() > 0.5) {
                Vector2f point1 = MathUtils.getRandomPointInCircle(ship.getLocation(), ((float) Math.random() * area * 0.25f + area * 0.25f) / PITCH_BEND.get(ship.getHullSize()));
                Vector2f point2 = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                int bound = 100;
                while (bound > 0) {
                    bound--;
                    point2 = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                    if (CollisionUtils.isPointWithinBounds(point2, ship)) {
                        break;
                    }
                }
                engine.spawnEmpArc(ship, point2, new SimpleEntity(point2), new SimpleEntity(point1), DamageType.ENERGY,
                        0f, 0f, 1000f, null, exploder.powerLevel * (exploder.chargeLevel * 20f + 20f), COLOR5, COLOR6);
            }
        }
        if (TEM_Util.isOnscreen(exploder.ship.getLocation(), ((float) Math.sqrt(exploder.powerLevel) * ship.getCollisionRadius() * 5f * (1f + exploder.powerLevel) + ship.getCollisionRadius()))) {
            for (int i = 0; i <= (int) (exploder.powerLevel * ship.getCollisionRadius()); i++) {
                float radius = shipRadius * (float) Math.random();
                Vector2f direction = MathUtils.getRandomPointOnCircumference(null, shipRadius * ((float) Math.random() * 4f + 1f) * (1f + exploder.powerLevel));
                Vector2f point = MathUtils.getPointOnCircumference(ship.getLocation(), radius, VectorUtils.getFacing(direction));
                engine.addSmoothParticle(point, direction, 15f * (float) Math.sqrt(exploder.powerLevel), 1f, (float) Math.sqrt(exploder.powerLevel) * 1f, COLOR5);
            }
        }

        engine.spawnExplosion(ship.getLocation(), ZERO, new Color(50, 150, 255, 150), area / 1.5f, unchargingTime * 1.5f);
        engine.addHitParticle(ship.getLocation(), ZERO, area * 2f, 10f, (float) Math.sqrt(unchargingTime), COLOR7);
        engine.addSmoothParticle(ship.getLocation(), ZERO, area * 2f, 10f, (float) Math.sqrt(unchargingTime), COLOR7);
        TEM_AnamorphicFlare.createFlare(ship, new Vector2f(ship.getLocation()), engine, 1f, 1f / (exploder.powerLevel * 40f), 0f, 0f, 2f, COLOR2, COLOR5);

        float totalDamage = 0f;
        List<ShipAPI> nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
        for (ShipAPI thisEnemy : nearbyEnemies) {
            if (thisEnemy.getCollisionClass() == CollisionClass.NONE || thisEnemy == ship) {
                continue;
            }

            float contribution = 1f;
            if (thisEnemy.isFighter() || thisEnemy.isDrone()) {
                contribution *= 0.25f;
            }

            float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
            if (thisEnemy.getOwner() == ship.getOwner()) {
                falloff *= 0.5f;
            }
            if (thisEnemy.getCollisionClass() == CollisionClass.NONE) {
                falloff *= 0.5f;
            } else {
                totalDamage += damage * falloff * contribution;
            }

            for (int i = 0; i <= (int) (damage * (falloff / 250f) / PITCH_BEND.get(ship.getHullSize())); i++) {
                totalDamage += damage * falloff * 0.25f * contribution;
            }
        }

        float attenuation = 1f;
        if (totalDamage > maxDamage) {
            attenuation *= maxDamage / totalDamage;
        }
        for (ShipAPI thisEnemy : nearbyEnemies) {
            if (thisEnemy.getCollisionClass() == CollisionClass.NONE || thisEnemy == ship) {
                continue;
            }

            Vector2f damagePoint = CollisionUtils.getCollisionPoint(ship.getLocation(), thisEnemy.getLocation(), thisEnemy);
            if (damagePoint == null) {
                damagePoint = thisEnemy.getLocation();
            }
            Vector2f forward = new Vector2f(damagePoint);
            forward.normalise();
            forward.scale(5f);
            Vector2f.add(forward, damagePoint, damagePoint);
            float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
            if (thisEnemy.getOwner() == ship.getOwner()) {
                falloff *= 0.5f;
            }
            if (thisEnemy.getCollisionClass() == CollisionClass.NONE) {
                falloff *= 0.5f;
            } else {
                engine.applyDamage(thisEnemy, damagePoint, damage * falloff * attenuation, DamageType.KINETIC, emp * falloff * attenuation, false, false, ship);
            }
            thisEnemy.getFluxTracker().increaseFlux(flux * falloff * attenuation, true);

            ShipAPI empTarget = thisEnemy;
            for (int i = 0; i <= Math.round(attenuation * damage * (falloff / 250f) / PITCH_BEND.get(ship.getHullSize())); i++) {
                Vector2f point = MathUtils.getRandomPointInCircle(thisEnemy.getLocation(), TEM_Util.effectiveRadius(thisEnemy) * 1.5f);
                engine.spawnEmpArc(ship, point, empTarget, empTarget, DamageType.KINETIC, damage * falloff * 0.25f, emp * falloff * 0.25f, 1000f, null, exploder.powerLevel * (float) Math.sqrt(damage), COLOR8, COLOR9);
            }
        }

        List<CombatEntityAPI> nearbyAsteroids = CombatUtils.getAsteroidsWithinRange(ship.getLocation(), area);
        for (CombatEntityAPI asteroid : nearbyAsteroids) {
            CombatUtils.applyForce(asteroid, VectorUtils.getDirectionalVector(ship.getLocation(), asteroid.getLocation()), force * (1f - MathUtils.getDistance(ship, asteroid) / area));
        }

        int numRepulseParticles = 0;
        for (DamagingProjectileAPI thisProj : engine.getProjectiles()) {
            if (thisProj.getOwner() != ship.getOwner()) {
                Vector2f thisProjLoc = thisProj.getLocation();
                Vector2f thisProjVel = thisProj.getVelocity();
                if (MathUtils.getDistanceSquared(ship.getLocation(), thisProjLoc) > (area * area)) {
                    continue;
                }

                if (numRepulseParticles < 20) {
                    numRepulseParticles++;
                    float scaleFactor;
                    if (thisProj.getDamageType() == DamageType.FRAGMENTATION) {
                        scaleFactor = (float) Math.sqrt(thisProj.getDamageAmount() * 0.25f);
                    } else {
                        scaleFactor = (float) Math.sqrt(thisProj.getDamageAmount() * 1f);
                    }

                    scaleFactor = Math.min(scaleFactor, 40f);

                    engine.addHitParticle(thisProjLoc, ship.getVelocity(), exploder.powerLevel * 3f * scaleFactor, 1f, (float) Math.sqrt(unchargingTime), COLOR5);
                }

                float returnAngle = VectorUtils.getAngle(ship.getLocation(), thisProjLoc);
                thisProjVel.set(MathUtils.getPointOnCircumference(null, thisProjVel.length(), returnAngle));

                thisProj.setFacing(returnAngle);
                if (!(thisProj instanceof MissileAPI)) {
                    thisProj.setOwner(ship.getOwner());
                    thisProj.setSource(ship);
                }
            }
        }

        StandardLight light = new StandardLight(ship.getLocation(), ZERO, ZERO, null);
        light.setColor(COLOR1);
        light.setSize(area * 1.5f);
        light.setIntensity(exploder.powerLevel);
        light.fadeOut(unchargingTime);
        LightShader.addLight(light);

        if (exploder.powerLevel >= 0.5f) {
            float time = 0.9f * (float) Math.sqrt(Math.sqrt(exploder.powerLevel / 2f)) / PITCH_BEND.get(ship.getHullSize());
            RippleDistortion ripple = new RippleDistortion(ship.getLocation(), ZERO);
            ripple.setSize(area);
            ripple.setIntensity(area * 0.15f);
            ripple.setFrameRate(60f / (time));
            ripple.fadeInSize(time);
            ripple.fadeOutIntensity(time);
            DistortionShader.addDistortion(ripple);
        }

        Global.getSoundPlayer().playSound("tem_excaliburdrive_blast", PITCH_BEND.get(ship.getHullSize()), 1.5f / PITCH_BEND.get(ship.getHullSize()), ship.getLocation(), ZERO);

        ship.setOwner(hulkOwner);

        /* Confettification */
        Set<ShipAPI> pieces = new HashSet<>(15);
        Set<ShipAPI> newPieces = new HashSet<>(15);
        pieces.add(ship);
        int count = CONFETTI_PIECES.get(ship.getHullSize());
        while (!pieces.isEmpty() && (count > 0)) {
            for (ShipAPI piece : pieces) {
                ShipAPI newPiece = piece.splitShip();
                if (newPiece != null) {
                    newPieces.add(newPiece);
                    ShipDestructionEffects.suppressEffects(newPiece, true, true);
                }
                count--;
                if (count <= 0) {
                    break;
                }
            }

            pieces.addAll(newPieces);
            newPieces.clear();
            count--;
        }

        nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
        for (ShipAPI thisEnemy : nearbyEnemies) {
            if (thisEnemy.getCollisionClass() == CollisionClass.NONE) {
                continue;
            }

            float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
            if (pieces.contains(thisEnemy)) {
                falloff *= 3f;
            } else {
                falloff *= attenuation;
            }
            CombatUtils.applyForce(TEM_Multi.getRoot(thisEnemy), VectorUtils.getDirectionalVector(ship.getLocation(), thisEnemy.getLocation()), force * falloff);
        }
    }

    private CombatEngineAPI engine;

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Set<ShipAPI> deadShips = localData.deadShips;
        final List<ExplodingShip> explodingShips = localData.explodingShips;

        Iterator<ExplodingShip> iter2 = explodingShips.iterator();
        while (iter2.hasNext()) {
            ExplodingShip exploder = iter2.next();
            ShipAPI ship = exploder.ship;

            if (ship == null || !engine.getShips().contains(ship)) {
                if (exploder.sound != null) {
                    exploder.sound.stop();
                }
                iter2.remove();
                continue;
            }

            float shipRadius = TEM_Util.effectiveRadius(ship);

            if (!exploder.playedSound) {
                exploder.playedSound = true;

                float powerLevel = 2f;
                float chargingTime = (float) Math.sqrt(powerLevel / 2f) * EXPLOSION_LENGTH.get(ship.getHullSize());
                exploder.sound = Global.getSoundPlayer().playSound("tem_excaliburdrive_charge", (float) Math.sqrt(4f / chargingTime), 1.2f / PITCH_BEND.get(ship.getHullSize()), ship.getLocation(), ship.getVelocity());
            }

            exploder.chargeLevel += amount * ship.getMutableStats().getTimeMult().getModifiedValue() / exploder.chargingTime;

            exploder.interval.advance(amount);
            if ((exploder.chargeLevel >= 1f) || (ship.isPiece())) {
                explode(engine, exploder);
                iter2.remove();
            } else if (exploder.interval.intervalElapsed()) {
                float force = exploder.powerLevel * 1.25f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux()) * amount * ship.getMutableStats().getTimeMult().getModifiedValue();
                float area = (float) Math.sqrt(exploder.powerLevel) * (float) Math.sqrt(ship.getCollisionRadius()) * 40f;

                if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_martyr") || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_archbishop")) {
                    force *= 2f;
                    area *= 2f;
                    List<ShipAPI> nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
                    for (ShipAPI thisEnemy : nearbyEnemies) {
                        if (thisEnemy == ship || thisEnemy.getOwner() == ship.getOwner()) {
                            continue;
                        }

                        float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
                        if (thisEnemy.getOwner() == ship.getOwner()) {
                            falloff *= 0.5f;
                        }

                        CombatUtils.applyForce(ship, VectorUtils.getDirectionalVector(ship.getLocation(), thisEnemy.getLocation()), force * falloff);
                    }
                } else {
                    List<ShipAPI> nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
                    for (ShipAPI thisEnemy : nearbyEnemies) {
                        if ((thisEnemy.getCollisionClass() == CollisionClass.NONE) || (thisEnemy == ship) || !TEM_Multi.isRoot(thisEnemy)) {
                            continue;
                        }

                        float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
                        if (thisEnemy.getOwner() == ship.getOwner()) {
                            falloff *= 0.5f;
                        }

                        CombatUtils.applyForce(thisEnemy, VectorUtils.getDirectionalVector(ship.getLocation(), thisEnemy.getLocation()), force * falloff);
                    }

                    List<CombatEntityAPI> nearbyAsteroids = CombatUtils.getAsteroidsWithinRange(ship.getLocation(), area);
                    for (CombatEntityAPI asteroid : nearbyAsteroids) {
                        CombatUtils.applyForce(asteroid, VectorUtils.getDirectionalVector(ship.getLocation(), asteroid.getLocation()), force * (1f - MathUtils.getDistance(ship, asteroid) / area));
                    }
                }

                if (TEM_Util.isOnscreen(exploder.ship.getLocation(), (exploder.chargingTime / 3f * ship.getCollisionRadius() * (1f + exploder.powerLevel) + 0.6f * ship.getCollisionRadius()))) {
                    for (int i = 0; i <= (int) (exploder.powerLevel * amount * ship.getMutableStats().getTimeMult().getModifiedValue() * ship.getCollisionRadius() * 0.5f); i++) {
                        float radius = 0.5f * shipRadius * ((float) Math.random() * 0.6f + 0.6f);
                        Vector2f direction = MathUtils.getRandomPointOnCircumference(null, shipRadius * (1f + exploder.powerLevel));
                        Vector2f point = MathUtils.getPointOnCircumference(ship.getLocation(), radius, VectorUtils.getFacing(direction));
                        Vector2f.add(ship.getVelocity(), direction, direction);
                        engine.addSmoothParticle(point, direction, 10f * (float) Math.sqrt(exploder.powerLevel), 1f, exploder.chargingTime / 3f, COLOR2);
                    }
                }
                for (int i = 0; i <= exploder.powerLevel * ship.getCollisionRadius() * 0.01f; i++) {
                    if (Math.random() > 0.8) {
                        Vector2f point1 = MathUtils.getRandomPointInCircle(ship.getLocation(), shipRadius * ((float) Math.random() + 1f) * 1.5f * exploder.chargeLevel);
                        Vector2f point2 = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                        int bound = 100;
                        while (bound > 0) {
                            bound--;
                            point2 = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                            if (CollisionUtils.isPointWithinBounds(point2, ship)) {
                                break;
                            }
                        }
                        if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_martyr") || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_archbishop")) {
                            engine.spawnEmpArc(ship, point2, new SimpleEntity(point2), new SimpleEntity(point1),
                                    DamageType.ENERGY, 0f, 0f, 1000f, null, exploder.powerLevel * (exploder.chargeLevel * 20f + 10f), COLOR10, COLOR11);
                        } else {
                            engine.spawnEmpArc(ship, point2, new SimpleEntity(point2), new SimpleEntity(point1),
                                    DamageType.ENERGY, 0f, 0f, 1000f, null, exploder.powerLevel * (exploder.chargeLevel * 20f + 10f), COLOR3, COLOR4);
                        }
                    }
                }
            }
        }

        List<ShipAPI> ships = engine.getShips();
        int shipsSize = ships.size();
        for (int i = 0; i < shipsSize; i++) {
            ShipAPI ship = ships.get(i);
            if ((ship == null) || !ship.getHullSpec().getHullId().startsWith("tem_")) {
                continue;
            }

            float shipRadius = TEM_Util.effectiveRadius(ship);

            if (ship.isHulk() && !ship.isPiece()) {
                if (!deadShips.contains(ship)) {
                    deadShips.add(ship);
                    if (EXPLOSION_CHANCE.containsKey(TEM_Util.getNonDHullId(ship.getHullSpec()))) {
                        if (Math.random() >= EXPLOSION_CHANCE.get(TEM_Util.getNonDHullId(ship.getHullSpec()))) {
                            continue;
                        }
                    }
                    if (!ship.isShuttlePod() && !ship.isDrone() && !ship.isFighter()) {
                        float powerLevel = 2f;
                        float chargingTime = (float) Math.sqrt(powerLevel / 2f) * EXPLOSION_LENGTH.get(ship.getHullSize());
                        StandardLight light = new StandardLight(ZERO, ZERO, ZERO, ship);
                        light.setColor(COLOR1);
                        light.setSize(shipRadius * 2f);
                        light.setIntensity(2f);
                        light.fadeIn(chargingTime);
                        light.setLifetime(0f);
                        LightShader.addLight(light);
                        ExplodingShip exploder = new ExplodingShip(ship, chargingTime, powerLevel);
                        explodingShips.add(exploder);

                        TEM_ExcaliburDrive.markMemberExploded(CombatUtils.getFleetMember(ship));
                    }
                }
            }
        }

        Iterator<ShipAPI> iter = deadShips.iterator();
        while (iter.hasNext()) {
            ShipAPI ship = iter.next();

            if (ship != null && !ships.contains(ship)) {
                iter.remove();
            }
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    private static final class ExplodingShip {

        float chargeLevel;
        float chargingTime;
        final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);
        boolean playedSound = false;
        float powerLevel;
        ShipAPI ship;
        SoundAPI sound;

        private ExplodingShip(ShipAPI ship, float chargingTime, float powerLevel) {
            this.ship = ship;
            this.chargingTime = chargingTime;
            this.powerLevel = powerLevel;
            this.chargeLevel = 0f;
        }
    }

    private static final class LocalData {

        final Set<ShipAPI> deadShips = new LinkedHashSet<>(50);
        final List<ExplodingShip> explodingShips = new ArrayList<>(50);
    }
}
