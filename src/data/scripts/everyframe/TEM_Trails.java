package data.scripts.everyframe;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.loading.ProjectileSpawnType;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.MagicTrailPlugin;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

public class TEM_Trails extends BaseEveryFrameCombatPlugin {

    private static final String MERCED_PROJECTILE_ID = "tem_merced_shot";
    private static final String PAX_PROJECTILE_ID = "tem_pax_shot";
    private static final String SENTENIA_PROJECTILE_ID = "tem_sentenia_shot";
    private static final String JUGER_PROJECTILE_ID = "tem_juger_shot";
    private static final String SECACE_PROJECTILE_ID = "tem_secace_shot";
    private static final String GALATINE_PROJECTILE_ID = "tem_galatine_shot";
    private static final String ARONDIGHT_PROJECTILE_ID = "tem_arondight_shot";
    private static final String JOYEUSE_PROJECTILE_ID = "tem_joyeuse_fake_shot";

    private static final Color MERCED_TRAIL_COLOR_START = new Color(255, 255, 200);
    private static final Color MERCED_TRAIL_COLOR_END = new Color(200, 200, 0);
    private static final Color PAX_TRAIL_COLOR_START = new Color(255, 255, 100);
    private static final Color PAX_TRAIL_COLOR_END = new Color(200, 200, 100);
    private static final Color SENTENIA_TRAIL_COLOR_START = new Color(255, 255, 150);
    private static final Color SENTENIA_TRAIL_COLOR_END = new Color(200, 200, 50);
    private static final Color JUGER_TRAIL_COLOR = new Color(246, 238, 139);
    private static final Color JUGER_TRAIL_COLOR2 = new Color(255, 230, 105);
    private static final Color SECACE_TRAIL_COLOR = new Color(50, 255, 255);
    private static final Color SECACE_TRAIL_COLOR2 = new Color(220, 240, 255);
    private static final Color GALATINE_TRAIL_COLOR = new Color(75, 230, 255);
    private static final Color GALATINE_TRAIL_COLOR2 = new Color(220, 240, 255);
    private static final Color ARONDIGHT_TRAIL_COLOR = new Color(100, 220, 255);
    private static final Color ARONDIGHT_TRAIL_COLOR2 = new Color(200, 230, 255);
    private static final Color JOYEUSE_TRAIL_COLOR = new Color(255, 25, 25);

    private static final float SIXTY_FPS = 1f / 60f;

    private static final String DATA_KEY = "TEM_Trails";

    private CombatEngineAPI engine;

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null) {
            return;
        }
        if (engine.isPaused()) {
            return;
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<DamagingProjectileAPI, TrailData> trailMap = localData.trailMap;

        List<DamagingProjectileAPI> projectiles = engine.getProjectiles();
        int size = projectiles.size();
        double trailCount = 0f;
        for (int i = 0; i < size; i++) {
            DamagingProjectileAPI projectile = projectiles.get(i);
            if (projectile.getProjectileSpecId() == null) {
                continue;
            }

            switch (projectile.getProjectileSpecId()) {
                case MERCED_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 0.25f)) {
                        trailCount += 1f;
                    }
                    break;
                case PAX_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 0.3f)) {
                        trailCount += 1f / 3f;
                    }
                    break;
                case SENTENIA_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 0.35f)) {
                        trailCount += 1f / 1.5f;
                    }
                    break;
                case JUGER_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 1f)) {
                        trailCount += 2f;
                    }
                    break;
                case SECACE_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 0.2f)) {
                        trailCount += 2f;
                    }
                    break;
                case GALATINE_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 0.3f)) {
                        trailCount += 3f;
                    }
                    break;
                case ARONDIGHT_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 0.5f)) {
                        trailCount += 4f;
                    }
                    break;
                case JOYEUSE_PROJECTILE_ID:
                    if (TEM_Util.isOnscreen(projectile.getLocation(), projectile.getVelocity().length() * 1f)) {
                        trailCount += 2f;
                    }
                    break;
                default:
                    break;
            }
        }

        float trailFPSRatio = Math.min(3f, (float) Math.max(1f, (trailCount / 30f)));

        for (int i = 0; i < size; i++) {
            DamagingProjectileAPI proj = projectiles.get(i);
            String spec = proj.getProjectileSpecId();
            TrailData data;
            if (spec == null) {
                continue;
            }

            boolean enableAngleFade = true;
            switch (spec) {
                case SENTENIA_PROJECTILE_ID:
                    enableAngleFade = false;
                    break;

                default:
                    break;
            }

            switch (spec) {
                case MERCED_PROJECTILE_ID:
                case PAX_PROJECTILE_ID:
                case SENTENIA_PROJECTILE_ID:
                case JUGER_PROJECTILE_ID:
                case SECACE_PROJECTILE_ID:
                case GALATINE_PROJECTILE_ID:
                case ARONDIGHT_PROJECTILE_ID:
                case JOYEUSE_PROJECTILE_ID:
                    data = trailMap.get(proj);
                    if (data == null) {
                        data = new TrailData();
                        data.id = MagicTrailPlugin.getUniqueID();

                        switch (spec) {
                            case JUGER_PROJECTILE_ID:
                            case SECACE_PROJECTILE_ID:
                                data.id2 = MagicTrailPlugin.getUniqueID();
                                break;

                            case GALATINE_PROJECTILE_ID:
                            case JOYEUSE_PROJECTILE_ID:
                                data.id2 = MagicTrailPlugin.getUniqueID();
                                data.id3 = MagicTrailPlugin.getUniqueID();
                                break;

                            case ARONDIGHT_PROJECTILE_ID:
                                data.id2 = MagicTrailPlugin.getUniqueID();
                                data.id3 = MagicTrailPlugin.getUniqueID();
                                data.id4 = MagicTrailPlugin.getUniqueID();
                                break;
                            default:
                                break;
                        }
                    }

                    trailMap.put(proj, data);
                    break;

                default:
                    continue;
            }

            if (!data.enabled) {
                continue;
            }

            float fade = 1f;
            if (proj.getBaseDamageAmount() > 0f) {
                fade = proj.getDamageAmount() / proj.getBaseDamageAmount();
            }

            if (enableAngleFade) {
                float velFacing = VectorUtils.getFacing(proj.getVelocity());
                float angleError = Math.abs(MathUtils.getShortestRotation(proj.getFacing(), velFacing));

                float angleFade = 1f - Math.min(Math.max(angleError - 45f, 0f) / 45f, 1f);
                fade *= angleFade;

                if (angleFade <= 0f) {
                    if (!data.cut) {
                        MagicTrailPlugin.cutTrailsOnEntity(proj);
                        data.cut = true;
                    }
                } else {
                    data.cut = false;
                }
            }

            if (fade <= 0f) {
                continue;
            }

            fade = Math.max(0f, Math.min(1f, fade));

            Vector2f projVel = new Vector2f(proj.getVelocity());
            Vector2f projBodyVel = VectorUtils.rotate(new Vector2f(projVel), -proj.getFacing());
            Vector2f projLateralBodyVel = new Vector2f(0f, projBodyVel.getY());
            Vector2f sidewaysVel = VectorUtils.rotate(new Vector2f(projLateralBodyVel), proj.getFacing());

            Vector2f spawnPosition = new Vector2f(proj.getLocation());
            if (proj.getSpawnType() != ProjectileSpawnType.BALLISTIC_AS_BEAM) {
                spawnPosition.x += sidewaysVel.x * amount * -1.05f;
                spawnPosition.y += sidewaysVel.y * amount * -1.05f;
            }

            switch (spec) {
                case MERCED_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS, SIXTY_FPS);
                    }
                    data.interval.advance(amount / trailFPSRatio);
                    if (data.interval.intervalElapsed()) {
                        float offset = 10f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        Map<String, Object> advancedOptions = new HashMap<>(2);
                        advancedOptions.put("SIZE_PULSE_WIDTH", 10f);
                        advancedOptions.put("SIZE_PULSE_COUNT", 2);
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_smoothtrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                30f, /* startSize */
                                10f, /* endSize */
                                MERCED_TRAIL_COLOR_START, /* startColor */
                                MERCED_TRAIL_COLOR_END, /* endColor */
                                fade * 0.35f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.25f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                advancedOptions /* advancedOptions */
                        );
                    }
                    break;

                case PAX_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS * 3f, SIXTY_FPS * 3f);
                    }
                    data.interval.advance(amount / trailFPSRatio);
                    if (data.interval.intervalElapsed()) {
                        float offset = 12.5f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        Map<String, Object> advancedOptions = new HashMap<>(2);
                        advancedOptions.put("SIZE_PULSE_WIDTH", 15f);
                        advancedOptions.put("SIZE_PULSE_COUNT", 2);
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_smoothtrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                25f, /* startSize */
                                10f, /* endSize */
                                PAX_TRAIL_COLOR_START, /* startColor */
                                PAX_TRAIL_COLOR_END, /* endColor */
                                fade * 0.3f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.3f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                advancedOptions /* advancedOptions */
                        );
                    }
                    break;

                case SENTENIA_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS * 1.5f, SIXTY_FPS * 1.5f);
                    }
                    data.interval.advance(amount / trailFPSRatio);
                    if (data.interval.intervalElapsed()) {
                        float offset = 17.5f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        Map<String, Object> advancedOptions = new HashMap<>(2);
                        advancedOptions.put("SIZE_PULSE_WIDTH", 10f);
                        advancedOptions.put("SIZE_PULSE_COUNT", 2);
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_smoothtrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                35f, /* startSize */
                                15f, /* endSize */
                                SENTENIA_TRAIL_COLOR_START, /* startColor */
                                SENTENIA_TRAIL_COLOR_END, /* endColor */
                                fade * 0.4f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.35f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                new Vector2f(0f, 0f), /* offsetVelocity */
                                advancedOptions /* advancedOptions */
                        );
                    }
                    break;

                case JUGER_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS, SIXTY_FPS);
                    }
                    data.interval.advance(amount);
                    if (data.interval.intervalElapsed()) {
                        float offset = 30f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_smoothtrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                50f, /* startSize */
                                20f, /* endSize */
                                JUGER_TRAIL_COLOR, /* startColor */
                                JUGER_TRAIL_COLOR, /* endColor */
                                fade, /* opacity */
                                0f, /* inDuration */
                                0.2f, /* mainDuration */
                                0.8f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        Map<String, Object> advancedOptions = new HashMap<>(2);
                        advancedOptions.put("SIZE_PULSE_WIDTH", 10f);
                        advancedOptions.put("SIZE_PULSE_COUNT", 3);
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id2, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_fuzzytrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                50f, /* startSize */
                                35f, /* endSize */
                                JUGER_TRAIL_COLOR2, /* startColor */
                                JUGER_TRAIL_COLOR2, /* endColor */
                                fade * 0.5f, /* opacity */
                                0.1f, /* inDuration */
                                0f, /* mainDuration */
                                0.5f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                300f, /* textureLoopLength */
                                500f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                advancedOptions /* advancedOptions */
                        );
                    }
                    break;

                case SECACE_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS, SIXTY_FPS);
                    }
                    data.interval.advance(amount);
                    if (data.interval.intervalElapsed()) {
                        float offset = 6f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_cleantrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                10f, /* startSize */
                                10f, /* endSize */
                                SECACE_TRAIL_COLOR, /* startColor */
                                SECACE_TRAIL_COLOR, /* endColor */
                                fade * 0.45f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.2f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id2, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_zappytrail"), /* sprite */
                                spawnPosition, /* position */
                                MathUtils.getRandomNumberInRange(0f, 200f), /* startSpeed */
                                MathUtils.getRandomNumberInRange(0f, 400f), /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                MathUtils.getRandomNumberInRange(-100f, 100f), /* startAngularVelocity */
                                MathUtils.getRandomNumberInRange(-200f, 200f), /* endAngularVelocity */
                                15f, /* startSize */
                                7.5f, /* endSize */
                                SECACE_TRAIL_COLOR2, /* startColor */
                                SECACE_TRAIL_COLOR2, /* endColor */
                                fade * 0.4f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.15f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                256f, /* textureLoopLength */
                                512f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                    }
                    break;

                case GALATINE_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS, SIXTY_FPS);
                    }
                    data.interval.advance(amount);
                    if (data.interval.intervalElapsed()) {
                        float offset = 15f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_cleantrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                15f, /* startSize */
                                15f, /* endSize */
                                GALATINE_TRAIL_COLOR, /* startColor */
                                GALATINE_TRAIL_COLOR, /* endColor */
                                fade * 0.7f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.3f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id2, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_zappytrail"), /* sprite */
                                spawnPosition, /* position */
                                MathUtils.getRandomNumberInRange(0f, 150f), /* startSpeed */
                                MathUtils.getRandomNumberInRange(0f, 300f), /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                MathUtils.getRandomNumberInRange(-100f, 100f), /* startAngularVelocity */
                                MathUtils.getRandomNumberInRange(-200f, 200f), /* endAngularVelocity */
                                25f, /* startSize */
                                12.5f, /* endSize */
                                GALATINE_TRAIL_COLOR2, /* startColor */
                                GALATINE_TRAIL_COLOR2, /* endColor */
                                fade * 0.7f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.25f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                256f, /* textureLoopLength */
                                512f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id3, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_zappytrail"), /* sprite */
                                spawnPosition, /* position */
                                MathUtils.getRandomNumberInRange(150f, 300f), /* startSpeed */
                                MathUtils.getRandomNumberInRange(300f, 600f), /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                MathUtils.getRandomNumberInRange(-100f, 100f), /* startAngularVelocity */
                                MathUtils.getRandomNumberInRange(-200f, 200f), /* endAngularVelocity */
                                20f, /* startSize */
                                10f, /* endSize */
                                GALATINE_TRAIL_COLOR2, /* startColor */
                                GALATINE_TRAIL_COLOR2, /* endColor */
                                fade * 0.5f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.2f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                256f, /* textureLoopLength */
                                512f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                    }
                    break;

                case ARONDIGHT_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS, SIXTY_FPS);
                    }
                    data.interval.advance(amount);
                    if (data.interval.intervalElapsed()) {
                        float offset = 50f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_cleantrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                20f, /* startSize */
                                20f, /* endSize */
                                ARONDIGHT_TRAIL_COLOR, /* startColor */
                                ARONDIGHT_TRAIL_COLOR, /* endColor */
                                fade * 0.8f, /* opacity */
                                0f, /* inDuration */
                                0.1f, /* mainDuration */
                                0.4f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id2, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_zappytrail"), /* sprite */
                                spawnPosition, /* position */
                                MathUtils.getRandomNumberInRange(0f, 150f), /* startSpeed */
                                MathUtils.getRandomNumberInRange(0f, 300f), /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                MathUtils.getRandomNumberInRange(-100f, 100f), /* startAngularVelocity */
                                MathUtils.getRandomNumberInRange(-200f, 200f), /* endAngularVelocity */
                                25f, /* startSize */
                                12.5f, /* endSize */
                                ARONDIGHT_TRAIL_COLOR2, /* startColor */
                                ARONDIGHT_TRAIL_COLOR2, /* endColor */
                                fade, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.35f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                256f, /* textureLoopLength */
                                512f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id3, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_zappytrail"), /* sprite */
                                spawnPosition, /* position */
                                MathUtils.getRandomNumberInRange(150f, 300f), /* startSpeed */
                                MathUtils.getRandomNumberInRange(300f, 600f), /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                MathUtils.getRandomNumberInRange(-100f, 100f), /* startAngularVelocity */
                                MathUtils.getRandomNumberInRange(-200f, 200f), /* endAngularVelocity */
                                20f, /* startSize */
                                10f, /* endSize */
                                ARONDIGHT_TRAIL_COLOR2, /* startColor */
                                ARONDIGHT_TRAIL_COLOR2, /* endColor */
                                fade * 0.8f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.3f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                256f, /* textureLoopLength */
                                512f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id4, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_zappytrail"), /* sprite */
                                spawnPosition, /* position */
                                MathUtils.getRandomNumberInRange(300f, 600f), /* startSpeed */
                                MathUtils.getRandomNumberInRange(600f, 1200f), /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                MathUtils.getRandomNumberInRange(-100f, 100f), /* startAngularVelocity */
                                MathUtils.getRandomNumberInRange(-200f, 200f), /* endAngularVelocity */
                                20f, /* startSize */
                                10f, /* endSize */
                                ARONDIGHT_TRAIL_COLOR2, /* startColor */
                                ARONDIGHT_TRAIL_COLOR2, /* endColor */
                                fade * 0.6f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.25f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                256f, /* textureLoopLength */
                                512f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                    }
                    break;

                case JOYEUSE_PROJECTILE_ID:
                    if (data.interval == null) {
                        data.interval = new IntervalUtil(SIXTY_FPS, SIXTY_FPS);
                    }
                    data.interval.advance(amount);
                    if (data.interval.intervalElapsed()) {
                        float offset = 2f;
                        Vector2f offsetPoint = new Vector2f((float) Math.cos(Math.toRadians(proj.getFacing())) * offset, (float) Math.sin(Math.toRadians(proj.getFacing())) * offset);
                        spawnPosition.x += offsetPoint.x;
                        spawnPosition.y += offsetPoint.y;

                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_smoothtrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180f, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                25f, /* startSize */
                                0f, /* endSize */
                                JOYEUSE_TRAIL_COLOR, /* startColor */
                                JOYEUSE_TRAIL_COLOR, /* endColor */
                                fade * 0.4f, /* opacity */
                                0f, /* inDuration */
                                0.2f, /* mainDuration */
                                1.2f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                -1f, /* textureLoopLength */
                                0f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                        MagicTrailPlugin.AddTrailMemberAdvanced(
                                proj, /* linkedEntity */
                                data.id2, /* ID */
                                Global.getSettings().getSprite("tem_trails", "tem_joytrail"), /* sprite */
                                spawnPosition, /* position */
                                0f, /* startSpeed */
                                0f, /* endSpeed */
                                proj.getFacing() - 180, /* angle */
                                0f, /* startAngularVelocity */
                                0f, /* endAngularVelocity */
                                40f, /* startSize */
                                40f, /* endSize */
                                JOYEUSE_TRAIL_COLOR, /* startColor */
                                JOYEUSE_TRAIL_COLOR, /* endColor */
                                fade * 0.6f, /* opacity */
                                0f, /* inDuration */
                                0f, /* mainDuration */
                                0.5f, /* outDuration */
                                GL11.GL_SRC_ALPHA, /* blendModeSRC */
                                GL11.GL_ONE, /* blendModeDEST */
                                80f, /* textureLoopLength */
                                40f, /* textureScrollSpeed */
                                sidewaysVel, /* offsetVelocity */
                                null /* advancedOptions */
                        );
                    }
                    break;

                default:
                    break;
            }
        }

        /* Clean up */
        Iterator<DamagingProjectileAPI> iter = trailMap.keySet().iterator();
        while (iter.hasNext()) {
            DamagingProjectileAPI proj = iter.next();
            if (!engine.isEntityInPlay(proj)) {
                iter.remove();
            }
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    private static final class LocalData {

        final Map<DamagingProjectileAPI, TrailData> trailMap = new LinkedHashMap<>(100);
    }

    private static final class TrailData {

        Float id = null;
        Float id2 = null;
        Float id3 = null;
        Float id4 = null;
        IntervalUtil interval = null;
        boolean cut = false;
        boolean enabled = true;
    }
}
