package data.scripts.world.templars;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin.DerelictShipData;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Items;
import com.fs.starfarer.api.impl.campaign.ids.StarTypes;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarGenDataSpec;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner.ShipRecoverySpecialCreator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipCondition;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin.DebrisFieldParams;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin.DebrisFieldSource;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin.MagneticFieldParams;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin.CoronaParams;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_Antioch {

    private static final Random random = new Random();

    public static void addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,
            ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipData params = new DerelictShipData(new PerShipData(variantId, condition), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitDays = orbitRadius / (10f + StarSystemGenerator.random.nextFloat() * 5f);
        ship.setCircularOrbit(focus, StarSystemGenerator.random.nextFloat() * 360f, orbitRadius, orbitDays);

        if (recoverable) {
            ShipRecoverySpecialCreator creator = new ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
            Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));
        }

    }

    public static void generatePt2(SectorAPI sector) {
        StarSystemAPI system = sector.getStarSystem("Antioch");
        MarketAPI ascalonMarket = sector.getEconomy().getMarket("tem_ascalon");

        pickLocation(sector, system);

        ascalonMarket.getLocationInHyperspace().set(system.getLocation());

        system.autogenerateHyperspaceJumpPoints(true, true, true);

        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);

        float minRadius = plugin.getTileSize() * 2f;
        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }

    public static SectorEntityToken getAscalon() {
        if (Global.getSector().getStarSystem("Antioch") == null) {
            return null;
        }
        return Global.getSector().getStarSystem("Antioch").getEntityById("tem_ascalon");
    }

    public static MarketAPI addMarketplace(String factionID, SectorEntityToken primaryEntity,
            ArrayList<SectorEntityToken> connectedEntities, String name, int size,
            ArrayList<String> conditionList, ArrayList<ArrayList<String>> industryList, ArrayList<String> submarkets,
            float tarrif, boolean freePort) {
        EconomyAPI globalEconomy = Global.getSector().getEconomy();
        String planetID = primaryEntity.getId();
        String marketID = planetID/* + "_market"*/;

        MarketAPI newMarket = Global.getFactory().createMarket(marketID, name, size);
        newMarket.setFactionId(factionID);
        newMarket.setPrimaryEntity(primaryEntity);
        newMarket.getTariff().modifyFlat("generator", tarrif);
        newMarket.getLocationInHyperspace().set(primaryEntity.getLocationInHyperspace());

        if (null != submarkets) {
            for (String market : submarkets) {
                newMarket.addSubmarket(market);
            }
        }

        for (String condition : conditionList) {
            newMarket.addCondition(condition);
        }

        for (ArrayList<String> industryWithParam : industryList) {
            String industry = industryWithParam.get(0);
            if (industryWithParam.size() == 1) {
                newMarket.addIndustry(industry);
            } else {
                newMarket.addIndustry(industry, industryWithParam.subList(1, industryWithParam.size()));
            }
        }

        if (null != connectedEntities) {
            for (SectorEntityToken entity : connectedEntities) {
                newMarket.getConnectedEntities().add(entity);
            }
        }

        newMarket.setFreePort(freePort);
        globalEconomy.addMarket(newMarket, true);
        primaryEntity.setMarket(newMarket);
        primaryEntity.setFaction(factionID);

        if (null != connectedEntities) {
            for (SectorEntityToken entity : connectedEntities) {
                entity.setMarket(newMarket);
                entity.setFaction(factionID);
            }
        }

        return newMarket;
    }

    private static float getRandom(float min, float max) {
        float radius = min + (max - min) * random.nextFloat();
        return radius;
    }

    private static void pickLocation(SectorAPI sector, StarSystemAPI system) {
        float radius = system.getMaxRadiusInHyperspace() + 200f;
        try_again:
        for (int i = 0; i < 100; i++) {
            Vector2f loc = new Vector2f(getRandom(17500f, 22500f), 0f);
            VectorUtils.rotate(loc, getRandom(0f, 360f), loc);

            for (LocationAPI location : sector.getAllLocations()) {
                if (location instanceof StarSystemAPI) {
                    float otherRadius = ((StarSystemAPI) location).getMaxRadiusInHyperspace();
                    if (MathUtils.getDistance(location.getLocation(), loc) < radius + otherRadius) {
                        continue try_again;
                    }
                }
            }

            system.getLocation().set(loc.x, loc.y);
            break;
        }
    }

    private static void setPulsar(StarSystemAPI system, PlanetAPI star) {
        StarCoronaTerrainPlugin coronaPlugin = Misc.getCoronaFor(star);
        if (coronaPlugin != null) {
            system.removeEntity(coronaPlugin.getEntity());
        }

        system.addCorona(star,
                300, // radius
                3, // wind
                0, // flares
                3); // cr loss

        StarGenDataSpec starData = (StarGenDataSpec) Global.getSettings().getSpec(StarGenDataSpec.class, star.getSpec().getPlanetType(), false);
        float corona = star.getRadius() * (starData.getCoronaMult() + starData.getCoronaVar() * (StarSystemGenerator.random.nextFloat() - 0.5f));
        if (corona < starData.getCoronaMin()) {
            corona = starData.getCoronaMin();
        }

        SectorEntityToken eventHorizon = system.addTerrain(Terrain.PULSAR_BEAM,
                new CoronaParams(star.getRadius() + corona, (star.getRadius() + corona) / 2f, star, starData.getSolarWind() / 4f,
                        starData.getMinFlare() + (starData.getMaxFlare() - starData.getMinFlare()) * StarSystemGenerator.random.nextFloat(), starData.getCrLossMult() / 2f));
        eventHorizon.setCircularOrbit(star, 0, 0, 100);
    }

    public void generate(SectorAPI sector) {
        random.setSeed(sector.getSeedString().hashCode());

        StarSystemAPI system = sector.createStarSystem("Antioch"); // 0.9 solar masses

        system.setBackgroundTextureFilename("graphics/templars/backgrounds/tem_antioch_background.jpg");

        PlanetAPI antioch = system.initStar("tem_antioch", StarTypes.NEUTRON_STAR, 50f, 400f);
        antioch.setCustomDescriptionId("star_antioch");
        setPulsar(system, antioch);

        SectorEntityToken antiochNebula1 = Misc.addNebulaFromPNG("data/campaign/terrain/tem_antioch_nebula_1.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "tem_nebula_antioch3", // "nebula_blue", // texture to use, uses xxx_map for map
                4, 4, StarAge.YOUNG); // number of cells in texture
        SectorEntityToken antiochNebula2 = Misc.addNebulaFromPNG("data/campaign/terrain/tem_antioch_nebula_2.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "tem_nebula_antioch2", // "nebula_blue", // texture to use, uses xxx_map for map
                4, 4, StarAge.YOUNG); // number of cells in texture
        SectorEntityToken antiochNebula3 = Misc.addNebulaFromPNG("data/campaign/terrain/tem_antioch_nebula_3.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "tem_nebula_antioch1", // "nebula_blue", // texture to use, uses xxx_map for map
                4, 4, StarAge.YOUNG); // number of cells in texture

        PlanetAPI ascalon = system.addPlanet("tem_ascalon", antioch, "Ascalon", "barren", 75f, 175f, 5000f, 385); // 1 AU
        ascalon.setFaction("templars");
        ascalon.setCustomDescriptionId("tem_ascalon");
        ascalon.getSpec().setPlanetColor(new Color(200, 255, 190, 255));
        ascalon.getSpec().setAtmosphereColor(new Color(100, 170, 200, 200));
        ascalon.getSpec().setCloudColor(new Color(150, 140, 100, 200));
        ascalon.getSpec().setTilt(80);
        ascalon.getSpec().setRotation(-10f);
        ascalon.getSpec().setAtmosphereThickness(0.1f);
        ascalon.applySpecChanges();

        SectorEntityToken ascalonField = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldParams(500f, // terrain effect band width
                        435f, // terrain effect middle radius
                        ascalon, // entity that it's around
                        185f, // visual band start
                        685f, // visual band end
                        new Color(70, 15, 30, 75), // base color
                        1f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(200, 20, 250),
                        new Color(100, 10, 160),
                        new Color(60, 140, 230),
                        new Color(70, 20, 230),
                        new Color(90, 40, 120),
                        new Color(160, 30, 70),
                        new Color(250, 50, 50)));
        ascalonField.setCircularOrbit(ascalon, 0, 0, 100);

        system.addAsteroidBelt(antioch, 100, 1000f, 1000f, 35, 35, Terrain.ASTEROID_BELT, null);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(255, 255, 255, 255), 1000f, 850, 35);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(245, 255, 245, 255), 1000f, 1150, 35);
        system.addAsteroidBelt(antioch, 250, 3000f, 2000f, 175, 185, Terrain.ASTEROID_BELT, null);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(225, 255, 225, 175), 1000f, 2550, 175);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(215, 255, 215, 175), 1000f, 2850, 180);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(205, 255, 205, 175), 1000f, 3150, 180);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(195, 255, 195, 175), 1000f, 3450, 185);
        system.addAsteroidBelt(antioch, 450, 6000f, 2500f, 700, 720, Terrain.ASTEROID_BELT, null);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(175, 255, 175, 100), 1000f, 5400, 700);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(165, 255, 165, 100), 1000f, 5700, 700);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(155, 255, 155, 100), 1000f, 6000, 705);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(145, 255, 145, 100), 1000f, 6300, 710);
        system.addRingBand(antioch, "misc", "tem_antioch_asteroids", 1024f, 0, new Color(135, 255, 135, 100), 1000f, 6600, 710);

        /* These poor sods were "cleansed" */
        int count = StarSystemGenerator.random.nextInt(10) + 10;
        for (int i = 0; i < count; i++) {
            float amount = StarSystemGenerator.random.nextFloat() * 200f + 100f;
            float density = StarSystemGenerator.random.nextFloat() * 0.5f + 0.5f;
            DebrisFieldParams params = new DebrisFieldParams(
                    amount / density, // field radius - should not go above 1000 for performance reasons
                    density / 3f, // density, visual - affects number of debris pieces
                    10000000f, // duration in days
                    0f); // days the field will keep generating glowing pieces
            params.source = DebrisFieldSource.BATTLE;
            params.baseSalvageXP = 500; // base XP for scavenging in field
            SectorEntityToken debris = Misc.addDebrisField(system, params, StarSystemGenerator.random);
            debris.setDiscoverable(true);
            debris.setSensorProfile(amount);

            float r = StarSystemGenerator.random.nextFloat();
            if (r < 0.2) {
                debris.setCircularOrbit(antioch, StarSystemGenerator.random.nextFloat() * 360f, StarSystemGenerator.random.nextFloat() * 300f + 850f, 35);
            } else if (r < 0.7) {
                debris.setCircularOrbit(antioch, StarSystemGenerator.random.nextFloat() * 360f, StarSystemGenerator.random.nextFloat() * 900f + 2550f, 180);
            } else {
                debris.setCircularOrbit(antioch, StarSystemGenerator.random.nextFloat() * 360f, StarSystemGenerator.random.nextFloat() * 1200f + 5400f, 705);
            }

            if (StarSystemGenerator.random.nextFloat() > 0.8f) {
                int wreckCount = Math.round(StarSystemGenerator.random.nextFloat() * amount / 50f);
                for (int j = 0; j < wreckCount; j++) {
                    ShipCondition condition;
                    if (StarSystemGenerator.random.nextBoolean()) {
                        condition = ShipCondition.WRECKED;
                    } else if (StarSystemGenerator.random.nextBoolean()) {
                        condition = ShipCondition.BATTERED;
                    } else if (StarSystemGenerator.random.nextBoolean()) {
                        condition = ShipCondition.AVERAGE;
                    } else if (StarSystemGenerator.random.nextBoolean()) {
                        condition = ShipCondition.GOOD;
                    } else {
                        condition = ShipCondition.PRISTINE;
                    }
                    addDerelict(system, debris, TEM_Util.derelictPicker.pick(StarSystemGenerator.random),
                            condition, StarSystemGenerator.random.nextFloat() * 150f + 50f, StarSystemGenerator.random.nextBoolean());
                }
            }
        }

        float radiusAfter = StarSystemGenerator.addOrbitingEntities(system, antioch, StarAge.YOUNG,
                2, 4, // min/max entities to add
                7000, // radius to start adding at
                0, // name offset - next planet will be <system name> <roman numeral of this parameter + 1>
                true); // whether to use custom or system-name based names

        MarketAPI ascalonMarket = addMarketplace("templars", ascalon,
                null,
                "Ascalon", 6, // 3 industry limit
                new ArrayList<>(Arrays.asList(
                        Conditions.POPULATION_6,
                        "tem_avalon",
                        Conditions.COLD,
                        Conditions.POOR_LIGHT,
                        Conditions.RUINS_EXTENSIVE,
                        Conditions.NO_ATMOSPHERE,
                        Conditions.METEOR_IMPACTS,
                        Conditions.ORE_ULTRARICH,
                        Conditions.RARE_ORE_ULTRARICH)),
                new ArrayList<>(Arrays.asList(
                        new ArrayList<>(Arrays.asList(Industries.POPULATION)),
                        new ArrayList<>(Arrays.asList(Industries.SPACEPORT)),
                        new ArrayList<>(Arrays.asList(Industries.MINING)), // Industry
                        new ArrayList<>(Arrays.asList(Industries.ORBITALWORKS, Items.PRISTINE_NANOFORGE)), // Industry
                        new ArrayList<>(Arrays.asList(Industries.HIGHCOMMAND, Commodities.ALPHA_CORE)), // Industry
                        new ArrayList<>(Arrays.asList(Industries.HEAVYBATTERIES, Commodities.ALPHA_CORE)),
                        new ArrayList<>(Arrays.asList(Industries.PLANETARYSHIELD, Commodities.ALPHA_CORE)))),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY,
                        Submarkets.SUBMARKET_STORAGE)),
                0f,
                false
        );

        SectorEntityToken word = system.addCustomEntity("tem_comm_antioch", "Sacred Word of Antioch", "tem_comm_network", "templars");
        word.setFixedLocation(99999f, 99999f);

        SharedData.getData().getMarketsWithoutTradeFleetSpawn().add(ascalonMarket.getId());

        if (system.hasTag(Tags.THEME_CORE_UNPOPULATED)) {
            system.removeTag(Tags.THEME_CORE_UNPOPULATED);
            system.addTag(Tags.THEME_CORE_POPULATED);
        }
    }
}
