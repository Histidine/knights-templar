package data.scripts.plugins;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import java.io.IOException;
import org.json.JSONException;

public class TEM_PriwenBurstPlugin extends BaseEveryFrameCombatPlugin {

    public static void activate(ShipAPI ship) {
        data.scripts.everyframe.TEM_PriwenBurstPlugin.activate(ship);
    }

    public static void addBursting(ShipAPI ship) {
        data.scripts.everyframe.TEM_PriwenBurstPlugin.addBursting(ship);
    }

    public static float chargeLevel(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.chargeLevel(ship);
    }

    public static float cooldownTime(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.cooldownTime(ship);
    }

    public static float effectLevel(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.effectLevel(ship);
    }

    public static float getExpectedArea(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.getExpectedArea(ship);
    }

    public static float getExpectedDamage(ShipAPI ship, float distance) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.getExpectedDamage(ship, distance);
    }

    public static float getExpectedFlux(ShipAPI ship, float distance) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.getExpectedFlux(ship, distance);
    }

    public static float getExpectedInvulnerabilityTime(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.getExpectedInvulnerabilityTime(ship);
    }

    public static float powerLevel(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.powerLevel(ship);
    }

    public static void reloadSettings() throws IOException, JSONException {
        data.scripts.everyframe.TEM_PriwenBurstPlugin.reloadSettings();
    }

    public static void removeFromCooldown(ShipAPI ship) {
        data.scripts.everyframe.TEM_PriwenBurstPlugin.removeFromCooldown(ship);
    }

    public static float trueCooldownTime(ShipAPI ship) {
        return data.scripts.everyframe.TEM_PriwenBurstPlugin.trueCooldownTime(ship);
    }
}
