package data.scripts.plugins;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.ShipAPI;

public class TEM_AlmaceBurstPlugin extends BaseEveryFrameCombatPlugin {

    public static void activate(ShipAPI ship) {
        data.scripts.everyframe.TEM_AlmaceBurstPlugin.activate(ship);
    }

    public static void addBursting(ShipAPI ship) {
        data.scripts.everyframe.TEM_AlmaceBurstPlugin.addBursting(ship);
    }

    public static float chargeLevel(ShipAPI ship) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.chargeLevel(ship);
    }

    public static float cooldownTime(ShipAPI ship) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.cooldownTime(ship);
    }

    public static float effectLevel(ShipAPI ship) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.effectLevel(ship);
    }

    public static float getExpectedArea(ShipAPI ship) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.getExpectedArea(ship);
    }

    public static float getExpectedDamage(ShipAPI ship, float distance) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.getExpectedDamage(ship, distance);
    }

    public static float getExpectedInvulnerabilityTime(ShipAPI ship) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.getExpectedInvulnerabilityTime(ship);
    }

    public static void removeFromCooldown(ShipAPI ship) {
        data.scripts.everyframe.TEM_AlmaceBurstPlugin.removeFromCooldown(ship);
    }

    public static float trueCooldownTime(ShipAPI ship) {
        return data.scripts.everyframe.TEM_AlmaceBurstPlugin.trueCooldownTime(ship);
    }
}
