package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_SenteniaEveryFrameEffect implements EveryFrameWeaponEffectPlugin {

    private static final float CHARGEUP_PARTICLE_ANGLE_SPREAD = 360f;
    private static final float CHARGEUP_PARTICLE_BRIGHTNESS = 1f;
    private static final Color CHARGEUP_PARTICLE_COLOR = new Color(255, 255, 175, 75);
    private static final float CHARGEUP_PARTICLE_COUNT_FACTOR = 7f;
    private static final float CHARGEUP_PARTICLE_DISTANCE_MAX = 40f;
    private static final float CHARGEUP_PARTICLE_DISTANCE_MIN = 20f;
    private static final float CHARGEUP_PARTICLE_DURATION = 0.3f;
    private static final float CHARGEUP_PARTICLE_SIZE_MAX = 15f;
    private static final float CHARGEUP_PARTICLE_SIZE_MIN = 7.5f;
    private static final float MUZZLE_OFFSET_HARDPOINT = 14.0f;
    private static final float MUZZLE_OFFSET_TURRET = 10.0f;

    private final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);
    private float lastChargeLevel = 0.0f;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (engine.isPaused()) {
            return;
        }

        float chargeLevel = weapon.getChargeLevel();

        if (chargeLevel > lastChargeLevel) {
            Vector2f weaponLocation = weapon.getLocation();
            ShipAPI ship = weapon.getShip();
            float shipFacing = weapon.getCurrAngle();
            Vector2f shipVelocity = ship.getVelocity();
            Vector2f muzzleLocation = MathUtils.getPointOnCircumference(weaponLocation, weapon.getSlot().isHardpoint() ?
                                                                                        MUZZLE_OFFSET_HARDPOINT :
                                                                                        MUZZLE_OFFSET_TURRET, shipFacing);

            interval.advance(amount);
            if (interval.intervalElapsed() && weapon.isFiring()) {
                int particleCount = (int) (CHARGEUP_PARTICLE_COUNT_FACTOR * chargeLevel);
                float distance, size, angle, speed;
                Vector2f particleVelocity;
                for (int i = 0; i < particleCount; ++i) {
                    distance = MathUtils.getRandomNumberInRange(CHARGEUP_PARTICLE_DISTANCE_MIN,
                                                                CHARGEUP_PARTICLE_DISTANCE_MAX);
                    size = MathUtils.getRandomNumberInRange(CHARGEUP_PARTICLE_SIZE_MIN, CHARGEUP_PARTICLE_SIZE_MAX);
                    angle = MathUtils.getRandomNumberInRange(-0.5f * CHARGEUP_PARTICLE_ANGLE_SPREAD, 0.5f *
                                                             CHARGEUP_PARTICLE_ANGLE_SPREAD);
                    Vector2f spawnLocation = MathUtils.getPointOnCircumference(muzzleLocation, distance, (angle +
                                                                                                          shipFacing));
                    speed = distance / CHARGEUP_PARTICLE_DURATION;
                    particleVelocity = MathUtils.getPointOnCircumference(shipVelocity, speed, 180.0f + angle +
                                                                         shipFacing);
                    engine.addHitParticle(spawnLocation, particleVelocity, size, CHARGEUP_PARTICLE_BRIGHTNESS,
                                          CHARGEUP_PARTICLE_DURATION,
                                          CHARGEUP_PARTICLE_COLOR);
                }
            }
        }

        lastChargeLevel = chargeLevel;
    }
}
