package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;

public class TEM_LancelotEveryFrameEffect implements EveryFrameWeaponEffectPlugin {

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (engine.isPaused()) {
            return;
        }

        ShipAPI ship = weapon.getShip();
        if (weapon.isFiring()) {
            float level = 1f - ship.getFluxTracker().getFluxLevel() * 0.75f;
            ship.getMutableStats().getBeamWeaponDamageMult().modifyMult("tem_lancelot_scaling", level);
            ship.getMutableStats().getBeamWeaponFluxCostMult().modifyMult("tem_lancelot_scaling", level);
        }
        else {
            ship.getMutableStats().getBeamWeaponDamageMult().unmodify("tem_lancelot_scaling");
            ship.getMutableStats().getBeamWeaponFluxCostMult().unmodify("tem_lancelot_scaling");
        }
    }
}
