package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import data.scripts.util.TEM_AnamorphicFlare;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;

public class TEM_ClarentOnHitEffect implements OnHitEffectPlugin {

    private static final Color CORE_COLOR = new Color(255, 255, 255, 200);
    private static final Color FLARE_COLOR = new Color(255, 150, 255, 200);
    private static final Vector2f ZERO = new Vector2f();

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        if (point == null) {
            return;
        }
        if (projectile.getSource() != null) {
            float angle = (float) Math.random() * 90f;
            TEM_AnamorphicFlare.createFlare(projectile.getSource(), new Vector2f(projectile.getLocation()), engine, 1f, 0.06f, angle, 10f, 2f,
                                            FLARE_COLOR, CORE_COLOR);
            TEM_AnamorphicFlare.createFlare(projectile.getSource(), new Vector2f(projectile.getLocation()), engine, 1f, 0.06f, angle + 90f, 10f, 2f,
                                            FLARE_COLOR, CORE_COLOR);
        }

        Global.getSoundPlayer().playSound("tem_clarent_impact", 1f, 1f, point, ZERO);
    }
}
