package data.scripts.weapons;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.util.TEM_Multi;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;

public class TEM_InterdictorBeamEffect implements BeamEffectPlugin {

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        CombatEntityAPI target = beam.getDamageTarget();
        if (target != null) {
            float realRadius = target.getCollisionRadius();
            if (target instanceof ShipAPI) {
                target = TEM_Multi.getRoot((ShipAPI) target);
                realRadius = TEM_Util.effectiveRadius((ShipAPI) target);
            }
            if (beam.getBrightness() >= 1f) {
                if (target.getCollisionClass() == CollisionClass.NONE) {
                    return;
                }
                if (target != beam.getSource()) {
                    float scale = beam.getBrightness();
                    float force = Math.max(40000f * scale / target.getMass(), 0.5f);
                    float angularForce = Math.max(3000000f * scale / (target.getMass() * realRadius), 0.5f);
                    Vector2f dir = new Vector2f(target.getVelocity());
                    if ((dir.x != 0f) || (dir.y != 0f)) {
                        dir.normalise();
                        dir.scale(-force * amount);
                        Vector2f.add(target.getVelocity(), dir, target.getVelocity());
                    }
                    if (target.getAngularVelocity() > 0f) {
                        target.setAngularVelocity(Math.max(target.getAngularVelocity() - angularForce * amount, 0f));
                    } else {
                        target.setAngularVelocity(Math.min(target.getAngularVelocity() + angularForce * amount, 0f));
                    }
                    if (beam.didDamageThisFrame()) {
                        engine.addSmoothParticle(target.getLocation(), target.getVelocity(), realRadius * 7.5f,
                                Math.min((float) Math.sqrt(force) * 0.04f, 0.25f), 0.3f,
                                new Color(100, 125, 150, Math.min((int) ((float) Math.sqrt(force) * 2f), 25)));
                    }
                }
            }
        }
    }
}
