package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.TEM_AnamorphicFlare;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_LancelotBeamEffect implements BeamEffectPlugin {

    private static final float BASE_WIDTH = 80f;
    private static final Color COLOR = new Color(255, 25, 25, 255);
    private static final float END_BIAS = 50f;
    private static final float MUZZLE_OFFSET_HARDPOINT = 14.0f;
    private static final float MUZZLE_OFFSET_TURRET = 14.0f;
    private static final float START_BIAS = 140f;
    private static final Vector2f ZERO = new Vector2f();

    public static Vector2f getRandomPointOnLine(Vector2f lineStart, Vector2f lineEnd, float startBias, float endBias) {
        final float t = startBias + ((float) Math.random() * (1f - (startBias + endBias)));
        return new Vector2f(lineStart.x + t * (lineEnd.x - lineStart.x), lineStart.y + t * (lineEnd.y - lineStart.y));
    }

    private boolean firing = false;
    private final IntervalUtil interval = new IntervalUtil(0.1f, 0.1f);
    private final IntervalUtil interval2 = new IntervalUtil(0.015f, 0.015f);
    private final IntervalUtil interval3 = new IntervalUtil(0.3f, 0.6f);
    private float level = 0f;
    private float sinceLast = 0f;
    private boolean wasZero = true;

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        Vector2f origin = new Vector2f(beam.getWeapon().getLocation());
        Vector2f offset = beam.getWeapon().getSlot().isHardpoint() ? new Vector2f(MUZZLE_OFFSET_HARDPOINT, 0f) : new Vector2f(MUZZLE_OFFSET_TURRET, 0f);
        VectorUtils.rotate(offset, beam.getWeapon().getCurrAngle(), offset);
        Vector2f.add(offset, origin, origin);

        ShipAPI ship = beam.getWeapon().getShip();
        float llvl = beam.getBrightness() * (1f - ship.getFluxTracker().getFluxLevel() * 0.75f);

        beam.setWidth(llvl * BASE_WIDTH * (1f - (float) Math.random() * 0.2f * llvl));

        beam.setFringeColor(new Color(255, 0, 0, TEM_Util.clamp255((int) (180.0 + Math.random() * 100.0 * llvl))));

        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        interval2.advance(amount);
        if (interval2.intervalElapsed()) {
            Global.getCombatEngine().addHitParticle(origin, ZERO, llvl * 250f, 0.3f, 5f * interval2.getIntervalDuration() * (float) Math.random(), COLOR);
        }

        if (firing) {
            if (beam.getBrightness() < level && beam.getBrightness() <= 0.8f) {
                firing = false;
                if (engine.getTotalElapsedTime(false) - sinceLast > 0.3f) {
                    Global.getSoundPlayer().playSound("tem_lancelot_end", 1f, 1.1f, origin, new Vector2f());
                }
            }
        } else {
            if (beam.getBrightness() > level) {
                firing = true;
                sinceLast = engine.getTotalElapsedTime(false);
            }
        }
        level = beam.getBrightness();

        float dur = beam.getDamage().getDpsDuration();
        if (!wasZero) {
            dur = 0;
        }
        wasZero = beam.getDamage().getDpsDuration() <= 0;
        if (dur > 0f) {
            CombatEntityAPI target = beam.getDamageTarget();
            List<CombatEntityAPI> asteroids = engine.getAsteroids();
            if ((beam.getBrightness() >= 1f) && (((target instanceof ShipAPI) && (((ShipAPI) target).isFighter() || ((ShipAPI) target).isDrone() || ((ShipAPI) target).isHulk())) || asteroids.contains((target)))) {
                float damageScaler = 9.9f;
                if (target instanceof ShipAPI) {
                    damageScaler = 2f;
                }
                engine.applyDamage(target, beam.getTo(), beam.getDamage().computeDamageDealt(dur) * damageScaler, DamageType.ENERGY, 0f, false, true, beam.getSource(), false);
            }
        }

        interval3.advance(amount * llvl * llvl);
        if (interval3.intervalElapsed()) {
            float beamLen = MathUtils.getDistance(beam.getFrom(), beam.getTo());
            if (beamLen > START_BIAS + END_BIAS) {
                Vector2f spawnLoc = getRandomPointOnLine(beam.getFrom(), beam.getTo(),
                        START_BIAS / beamLen, END_BIAS / beamLen);
                float spawnAngle = beam.getWeapon().getCurrAngle();
                if (Math.random() > 0.5) {
                    spawnAngle += MathUtils.getRandomNumberInRange(5f, 100f);
                } else {
                    spawnAngle -= MathUtils.getRandomNumberInRange(5f, 100f);
                }
                spawnAngle = MathUtils.clampAngle(spawnAngle);
                engine.spawnProjectile(ship, beam.getWeapon(), "tem_joyeuse", spawnLoc, spawnAngle, null);
                Global.getSoundPlayer().playSound("tem_joyeuse_split", 1.15f, 0.75f, spawnLoc, ZERO);

                TEM_AnamorphicFlare.createFlare(ship, spawnLoc, engine,
                        0.2f, 0.15f, spawnAngle, 0f, 4f,
                        new Color(255, TEM_Util.clamp255((int) MathUtils.getRandomNumberInRange(25f, 50f)), TEM_Util.clamp255((int) MathUtils.getRandomNumberInRange(25f, 50f))),
                        new Color(255, TEM_Util.clamp255((int) MathUtils.getRandomNumberInRange(50f, 100f)), TEM_Util.clamp255((int) MathUtils.getRandomNumberInRange(50f, 100f))));
            }
        }

        interval.advance(amount);
        if (interval.intervalElapsed()) {
            if (beam.getDamageTarget() != null) {
                Global.getCombatEngine().spawnExplosion(new Vector2f(beam.getTo()), ZERO,
                        new Color(255, TEM_Util.clamp255(50 + (int) ((float) Math.random() * 50f)), TEM_Util.clamp255(50 + (int) ((float) Math.random() * 25f)), 100), llvl * 80f, 0.2f);
                for (int x = 0; x < Math.round(15 * llvl); x++) {
                    float angle = VectorUtils.getAngle(beam.getTo(), beam.getDamageTarget().getLocation()) + 180f + (float) Math.random() * 210f - 105f;
                    if (angle >= 360f) {
                        angle -= 360f;
                    } else if (angle < 0f) {
                        angle += 360f;
                    }
                    engine.addHitParticle(new Vector2f(beam.getTo()),
                            MathUtils.getPointOnCircumference(null, ((float) Math.random() * 500f + 250f) * llvl, angle),
                            (10f + (float) Math.random() * 5f) * llvl, 1f,
                            (float) Math.random() * 0.4f + (0.4f * llvl),
                            new Color(255, TEM_Util.clamp255(50 + (int) ((float) Math.random() * 50f)), TEM_Util.clamp255(50 + (int) ((float) Math.random() * 25f)), 255));
                }
            }
        }
    }

}
