package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import data.scripts.everyframe.TEM_WeaponScriptPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_CrucifixOnHitEffect implements OnHitEffectPlugin {

    private static final Color EMP_COLOR = new Color(150, 230, 255);
    private static final Color EXPLOSION_COLOR = new Color(100, 220, 255, 150);
    private static final float FLUX_DAMAGE = 100f;
    private static final int NUM_PARTICLES = 12;
    private static final Color PARTICLE_COLOR = new Color(10, 200, 255, 200);
    private static final Vector2f ZERO = new Vector2f();

    @SuppressWarnings("AssignmentToMethodParameter")
    public static void explode(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point,
            boolean shieldHit, CombatEngineAPI engine) {
        boolean hitShields = shieldHit;
        if (point == null) {
            point = projectile.getLocation();
        }

//        if (target == null) {
//            engine.spawnExplosion(point, ZERO, EXPLOSION_COLOR, 100f, 0.25f);
//        } else {
//            engine.spawnExplosion(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f),
//                    EXPLOSION_COLOR, 100f, 0.25f);
//        }
        float speed = projectile.getVelocity().length();
        for (int x = 0; x < NUM_PARTICLES; x++) {
            float angle;
            if (target == null) {
                angle = (float) Math.random() * 360f;
            } else {
                angle = projectile.getFacing() + 180f + (float) Math.random() * 210f - 105f;
            }
            if (angle >= 360f) {
                angle -= 360f;
            } else if (angle < 0f) {
                angle += 360f;
            }
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(
                    speed * 0.05f, speed * 0.25f),
                    angle), 10f, 0.75f,
                    MathUtils.getRandomNumberInRange(0.25f, 0.5f),
                    PARTICLE_COLOR);
        }

        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;

            if (!hitShields) {
                if (ship.getVariant().getHullMods().contains("tem_latticeshield")
                        && TEM_LatticeShield.shieldLevel(ship) > 0f) {
                    hitShields = true;
                }
            }

            ship.getFluxTracker().increaseFlux(FLUX_DAMAGE, hitShields);

            float pierceChance = 0.25f;
            pierceChance *= ship.getMutableStats().getDynamic().getValue(Stats.SHIELD_PIERCED_MULT);

            boolean piercedShield = hitShields && (float) Math.random() < pierceChance;
            if (!hitShields || piercedShield) {
                ShipAPI empTarget = ship;
                float emp = projectile.getEmpAmount() * 0.5f;
                float dam = projectile.getDamageAmount() * 0.5f;
                engine.spawnEmpArcPierceShields(projectile.getSource(), point, empTarget, empTarget, DamageType.ENERGY,
                        dam, emp, 100000f, null, 20f,
                        PARTICLE_COLOR, EMP_COLOR);
            }
        }

        float angle = (float) Math.random() * 360f;
        float distance = (float) Math.random() * 35f + 25f;
        Vector2f point1 = MathUtils.getPointOnCircumference(point, distance, angle);
        Vector2f point2 = new Vector2f(point);
        engine.spawnEmpArc(projectile.getSource(), point1, new SimpleEntity(point1), new SimpleEntity(point2),
                DamageType.ENERGY, 0f, 0f, 1000f, null, 15f,
                PARTICLE_COLOR, EXPLOSION_COLOR);

        if (target != null) {
            TEM_WeaponScriptPlugin.genCrucifixBlast(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f));
        } else {
            TEM_WeaponScriptPlugin.genCrucifixBlast(point, ZERO);
        }

        Global.getSoundPlayer().playSound("tem_crucifix_impact", 1f, 0.85f, point, ZERO);
    }

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit,
            CombatEngineAPI engine) {
        explode(projectile, target, point, shieldHit, engine);
    }
}
