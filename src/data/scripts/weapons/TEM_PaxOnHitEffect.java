package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import data.scripts.everyframe.TEM_WeaponScriptPlugin;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_PaxOnHitEffect implements OnHitEffectPlugin {

    private static final int NUM_PARTICLES = 5;
    private static final Color PARTICLE_COLOR = new Color(255, 255, 100, 150);
    private static final Vector2f ZERO = new Vector2f();

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        if (point == null) {
            return;
        }

        float speed = projectile.getVelocity().length() + 300f;
        for (int x = 0; x < NUM_PARTICLES; x++) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(speed * 0.05f, speed * 0.3f),
                    (float) Math.random() * 360f), 5f, 1f, MathUtils.getRandomNumberInRange(0.3f, 1f),
                    PARTICLE_COLOR);
        }

        float timeAfterFade = Math.max(0f,
                projectile.getElapsed() - (projectile.getWeapon().getRange() / projectile.getWeapon().getProjectileSpeed()));
        float alpha = (0.5f - timeAfterFade) / 0.5f;
        if (target != null) {
            TEM_WeaponScriptPlugin.genPaxBlast(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f), alpha);
        } else {
            TEM_WeaponScriptPlugin.genPaxBlast(point, ZERO, alpha);
        }
    }
}
