package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_ArondightEveryFrameEffect implements EveryFrameWeaponEffectPlugin {

    private static final float CHARGEUP_PARTICLE_ANGLE_SPREAD = 360f;
    private static final float CHARGEUP_PARTICLE_BRIGHTNESS = 1f;
    private static final Color CHARGEUP_PARTICLE_COLOR = new Color(175, 210, 255, 100);
    private static final float CHARGEUP_PARTICLE_COUNT_FACTOR = 20f;
    private static final float CHARGEUP_PARTICLE_DISTANCE_MAX = 125f;
    private static final float CHARGEUP_PARTICLE_DISTANCE_MIN = 20f;
    private static final float CHARGEUP_PARTICLE_DURATION = 0.75f;
    private static final float CHARGEUP_PARTICLE_SIZE_MAX = 10f;
    private static final float CHARGEUP_PARTICLE_SIZE_MIN = 5f;
    private static final Color MUZZLE_FLASH_COLOR = new Color(180, 230, 255, 200);
    private static final float MUZZLE_FLASH_DURATION = 0.25f;
    private static final float MUZZLE_FLASH_SIZE = 250.0f;
    private static final float MUZZLE_OFFSET_HARDPOINT = 28.5f;
    private static final float MUZZLE_OFFSET_TURRET = 30.0f;

    private final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);
    private float lastChargeLevel = 0.0f;
    private int lastWeaponAmmo = 0;
    private boolean shot = false;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (engine.isPaused()) {
            return;
        }

        float chargeLevel = weapon.getChargeLevel();
        int weaponAmmo = weapon.getAmmo();

        if ((chargeLevel > lastChargeLevel) || (weaponAmmo < lastWeaponAmmo)) {
            Vector2f weaponLocation = weapon.getLocation();
            ShipAPI ship = weapon.getShip();
            float shipFacing = weapon.getCurrAngle();
            Vector2f shipVelocity = ship.getVelocity();
            Vector2f muzzleLocation = MathUtils.getPointOnCircumference(weaponLocation,
                    weapon.getSlot().isHardpoint() ? MUZZLE_OFFSET_HARDPOINT : MUZZLE_OFFSET_TURRET, shipFacing);

            interval.advance(amount);

            if (interval.intervalElapsed() && weapon.isFiring()) {
                int particleCount = (int) (CHARGEUP_PARTICLE_COUNT_FACTOR * chargeLevel);
                float distance, size, angle, speed;
                Vector2f particleVelocity;
                for (int i = 0; i < particleCount; ++i) {
                    distance = MathUtils.getRandomNumberInRange(CHARGEUP_PARTICLE_DISTANCE_MIN, CHARGEUP_PARTICLE_DISTANCE_MAX);
                    size = MathUtils.getRandomNumberInRange(CHARGEUP_PARTICLE_SIZE_MIN, CHARGEUP_PARTICLE_SIZE_MAX);
                    angle = MathUtils.getRandomNumberInRange(-0.5f * CHARGEUP_PARTICLE_ANGLE_SPREAD, 0.5f * CHARGEUP_PARTICLE_ANGLE_SPREAD);
                    Vector2f spawnLocation = MathUtils.getPointOnCircumference(muzzleLocation, distance, (angle + shipFacing));
                    speed = distance / CHARGEUP_PARTICLE_DURATION;
                    particleVelocity = MathUtils.getPointOnCircumference(shipVelocity, speed, 180.0f + angle + shipFacing);
                    engine.addHitParticle(spawnLocation, particleVelocity, size, CHARGEUP_PARTICLE_BRIGHTNESS * weapon.getChargeLevel(),
                            CHARGEUP_PARTICLE_DURATION, CHARGEUP_PARTICLE_COLOR);
                }
                Vector2f point1 = MathUtils.getRandomPointInCircle(muzzleLocation, (float) Math.random() * weapon.getChargeLevel() * 125f + 50f);
                engine.spawnEmpArc(ship, muzzleLocation, new SimpleEntity(muzzleLocation), new SimpleEntity(point1),
                        DamageType.ENERGY, 0f, 0f, 1000f, null, weapon.getChargeLevel() * 5f + 5f, CHARGEUP_PARTICLE_COLOR, CHARGEUP_PARTICLE_COLOR);
            }

            if (!shot && (weaponAmmo < lastWeaponAmmo)) {
                engine.spawnExplosion(muzzleLocation, shipVelocity, MUZZLE_FLASH_COLOR, MUZZLE_FLASH_SIZE, MUZZLE_FLASH_DURATION);
                engine.addSmoothParticle(muzzleLocation, shipVelocity, MUZZLE_FLASH_SIZE * 3f, 1f, MUZZLE_FLASH_DURATION * 2f, MUZZLE_FLASH_COLOR);

                for (int i = 0; i < 5; ++i) {
                    Vector2f point1 = MathUtils.getRandomPointInCircle(muzzleLocation, (float) Math.random() * 150f + 150f);
                    engine.spawnEmpArc(ship, muzzleLocation, new SimpleEntity(muzzleLocation), new SimpleEntity(point1),
                            DamageType.ENERGY, 0f, 0f, 1000f, null, weapon.getChargeLevel() * 15f + 15f, CHARGEUP_PARTICLE_COLOR, MUZZLE_FLASH_COLOR);
                }

                RippleDistortion ripple = new RippleDistortion(muzzleLocation, ship.getVelocity());
                ripple.setSize(MUZZLE_FLASH_SIZE);
                ripple.setIntensity(MUZZLE_FLASH_SIZE * 0.1f);
                ripple.setFrameRate(60f / (MUZZLE_FLASH_DURATION * 2f));
                ripple.fadeInSize(MUZZLE_FLASH_DURATION * 2f);
                ripple.fadeOutIntensity(MUZZLE_FLASH_DURATION * 2f);
                DistortionShader.addDistortion(ripple);

                float oppositeAngle = weapon.getCurrAngle() - 180f;
                if (oppositeAngle < 0f) {
                    oppositeAngle += 360f;
                }
                CombatUtils.applyForce(ship, oppositeAngle, 500f);
            } else {
                shot = false;
            }
        } else {
            shot = false;
        }

        lastChargeLevel = chargeLevel;
        lastWeaponAmmo = weaponAmmo;
    }
}
