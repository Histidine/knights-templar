#version 110

uniform sampler2D tex;
uniform sampler2D buf;
uniform sampler1D data;
uniform float trans;
uniform int size;
uniform vec4 norm1;
uniform vec2 norm2;

vec2 coord = gl_TexCoord[0].xy;

// x = position.x (0)  |  y = position.y (1)  |  z = size (2)  |  w = intensity (3)
vec4 getHitData(in float index) {
	// This is equivalent to: float texCoord = (4.0 * index + 0.5) / 4096.0;
	float texCoord = (0.0009765625 * index) + 0.0001220703125;
	return vec4((texture1D(data, texCoord).r * norm1.x) + norm1.y, (texture1D(data, texCoord + 0.000244140625).r * norm1.z) + norm1.w, texture1D(data, texCoord + 0.00048828125).r * norm2.x, texture1D(data, texCoord + 0.000732421875).r * norm2.y);
}

void main() {
	vec3 color = texture2D(tex, coord).xyz;
	vec4 color2 = texture2D(buf, coord);
	color2.a *= 2.0;

	float alpha = (color2.r + color2.g + color2.b) * 0.333333;
	if (alpha > 0.0) {
		float illum = 0.0;

		vec4 hitData;
		for (int i = 0; i < size; i++) {
			hitData = getHitData(float(i));

			vec2 d = coord - hitData.xy;
			d.x = d.x * trans;
			float distance = dot(d, d);

			if (distance < hitData.z * hitData.z) {
				float magnitude = hitData.w * (hitData.z - sqrt(distance)) / hitData.z;
				illum += magnitude;
			}
		}

		illum = sqrt(illum * 0.3) * alpha * 5.0;
		vec3 result = color2.rgb * max(illum, color2.a);
		color2.rgb = color2.rgb * 0.75 + max(illum, color2.a) * 0.25;
		vec3 diff = max(color2.rgb - color, 0.0);
		diff = sqrt(diff);
		result = result * diff + color;
		color = mix(color, result, alpha);
	}

	gl_FragColor = vec4(color.r, color.g, color.b, 1.0);
}
