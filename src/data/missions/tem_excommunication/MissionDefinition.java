package data.missions.tem_excommunication;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        api.initFleet(FleetSide.PLAYER, "", FleetGoal.ATTACK, false, 6);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true, 2);

        api.setFleetTagline(FleetSide.PLAYER, "Templar strike fleet");
        api.setFleetTagline(FleetSide.ENEMY, "Ragesh III garrison and mercenary allies");

        api.addBriefingItem("Defeat all enemy forces");
        api.addBriefingItem("The Estoc must survive");

        api.defeatOnShipLoss("Estoc");
        api.addToFleet(FleetSide.PLAYER, "tem_chevalier_ati", FleetMemberType.SHIP, "Estoc", true).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.PLAYER, "tem_crusader_def", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tem_crusader_est", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tem_jesuit_est", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tem_jesuit_sal", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tem_martyr_est", FleetMemberType.SHIP, false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.PLAYER, "tem_martyr_est", FleetMemberType.SHIP, false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.PLAYER, "tem_martyr_sal", FleetMemberType.SHIP, false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.PLAYER, "tem_martyr_sal", FleetMemberType.SHIP, false).getCaptain().setPersonality(
                "aggressive");

        api.addToFleet(FleetSide.ENEMY, "conquest_Elite", FleetMemberType.SHIP, "CSS Song of Roland", false);
        api.addToFleet(FleetSide.ENEMY, "apogee_Balanced", FleetMemberType.SHIP, "CSS Dominatrix", false);
        api.addToFleet(FleetSide.ENEMY, "sunder_Assault", FleetMemberType.SHIP, "CSS Sharkeater", false);
        api.addToFleet(FleetSide.ENEMY, "heron_Attack", FleetMemberType.SHIP, "CSS Bellerophon", false);
        api.addToFleet(FleetSide.ENEMY, "brawler_Elite", FleetMemberType.SHIP, "CSS Indomitable", false);
        api.addToFleet(FleetSide.ENEMY, "brawler_Elite", FleetMemberType.SHIP, "CSS Reliant", false);
        api.addToFleet(FleetSide.ENEMY, "eagle_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "dominator_Outdated", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "monitor_Escort", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "monitor_Escort", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "lasher_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "lasher_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);

        float width = 30000f;
        float height = 15000f;
        api.initMap(-width / 2f, width / 2f, -height / 2f, height / 2f);

        float minX = -width / 2;
        float minY = -height / 2;

        for (int i = 0; i < 15; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 100f + (float) Math.random() * 900f;
            api.addNebula(x, y, radius);
        }

        api.addNebula(minX + width * 0.3f - 1000, minY + height * 0.4f, 2000);
        api.addNebula(minX + width * 0.3f - 1000, minY + height * 0.5f, 2000);
        api.addNebula(minX + width * 0.3f - 1000, minY + height * 0.6f, 2000);

        api.addObjective(minX + width * 0.8f - 1000, minY + height * 0.4f, "nav_buoy");
        api.addObjective(minX + width * 0.8f - 1000, minY + height * 0.6f, "nav_buoy");
        api.addObjective(minX + width * 0.3f + 1000, minY + height * 0.3f, "comm_relay");
        api.addObjective(minX + width * 0.3f + 1000, minY + height * 0.7f, "comm_relay");
        api.addObjective(minX + width * 0.5f, minY + height * 0.5f, "sensor_array");
        api.addObjective(minX + width * 0.2f + 1000, minY + height * 0.5f, "sensor_array");

        api.addAsteroidField(minX + width * 0.3f, minY, 90, 3000f, 20f, 70f, 50);

        api.addPlanet(0, 0, 300f, "jungle", 300f, true);
    }
}
