package data.missions.tem_ahardplace;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        api.initFleet(FleetSide.PLAYER, "PLSS", FleetGoal.ATTACK, false, 4);
        api.initFleet(FleetSide.ENEMY, "", FleetGoal.ATTACK, true, 4);

        api.setFleetTagline(FleetSide.PLAYER, "Persean League security detachment");
        api.setFleetTagline(FleetSide.ENEMY, "Templar incursion");

        api.addBriefingItem("Defeat all enemy forces");
        api.addBriefingItem("PLSS Stoneskin must survive");

        api.defeatOnShipLoss("PLSS Stoneskin");
        api.addToFleet(FleetSide.PLAYER, "onslaught_Elite", FleetMemberType.SHIP, "PLSS Stoneskin", true);
        api.addToFleet(FleetSide.PLAYER, "falcon_CS", FleetMemberType.SHIP, "PLSS Eldritch Blast", false);
        api.addToFleet(FleetSide.PLAYER, "falcon_CS", FleetMemberType.SHIP, "PLSS Shillelagh", false);
        api.addToFleet(FleetSide.PLAYER, "medusa_Attack", FleetMemberType.SHIP, "PLSS Fireball", false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_Elite", FleetMemberType.SHIP, "PLSS Flaming Sphere", false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_Elite", FleetMemberType.SHIP, "PLSS Scorching Ray", false);
        api.addToFleet(FleetSide.PLAYER, "condor_Attack", FleetMemberType.SHIP, "PLSS Silence", false);
        api.addToFleet(FleetSide.PLAYER, "monitor_Escort", FleetMemberType.SHIP, "PLSS Shield of Faith", false);
        api.addToFleet(FleetSide.PLAYER, "monitor_Escort", FleetMemberType.SHIP, "PLSS Sanctuary", false);

        api.addToFleet(FleetSide.ENEMY, "tem_paladin_est", FleetMemberType.SHIP, "Greatsword", true).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_chevalier_est", FleetMemberType.SHIP, "Flamberge", false).getCaptain().setPersonality(
                "cautious");
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_est", FleetMemberType.SHIP, "Sidesword", false);
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_est", FleetMemberType.SHIP, "Rapier", false);
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_sal", FleetMemberType.SHIP, "Pike", false);
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_sal", FleetMemberType.SHIP, "Spear", false);

        float width = 20000f;
        float height = 12000f;
        api.initMap(-width / 2f, width / 2f, -height / 2f, height / 2f);

        for (int i = 0; i < 30; i++) {
            float x = (float) Math.random() * -width * 0.5f - width * 0.05f;
            float y = (float) Math.random() * height - height * 0.5f;
            float radius = 250f + (float) Math.random() * 750f;
            api.addNebula(x, y, radius);
        }

        api.addNebula(-width * 0.3f, 0, width * 0.2f);

        int minAsteroidSpeed = 30;
        int asteroidCount = 300;

        api.addAsteroidField(width * 0.3f,
                             0,
                             90,
                             width * 0.5f,
                             minAsteroidSpeed,
                             minAsteroidSpeed * 1.2f,
                             asteroidCount);

        api.addObjective(-width * 0.25f, 0, "nav_buoy");
        api.addObjective(0, 0, "comm_relay");
        api.addObjective(width * 0.25f, 0, "sensor_array");

        api.addPlanet(-500f, -250f, 100f, "tundra", 100f, true);
    }
}
