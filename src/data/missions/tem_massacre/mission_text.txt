Location: Inside the Coral Nebula
Date: 153.7.6

A year after her victory in the Coral Nebula against a superior Hegemony force, a certain Tri-Tachyon fleet commander returns to the site of her fateful battle in an attempt to corner a pair of advanced raider frigates aligned with The Knights Templar. Corporate Command has issued a kill order on these frigates, superceding the "capture intact" directive due to the rising cost in damages. Bringing an entire Tri-Tachyon carrier fleet to bear, the commander commits everything to the engagement so the two frigates will have nowhere to run.

Little does the Tri-Tachyon fleet commander know that those frigates have been waiting for precisely this moment...