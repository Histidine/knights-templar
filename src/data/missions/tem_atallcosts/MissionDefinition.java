package data.missions.tem_atallcosts;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import java.util.Random;

public class MissionDefinition implements MissionDefinitionPlugin {

    Random rand = new Random();

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        api.initFleet(FleetSide.PLAYER, "SS", FleetGoal.ATTACK, false, 4);
        api.initFleet(FleetSide.ENEMY, "", FleetGoal.ATTACK, true, 4);

        api.setFleetTagline(FleetSide.PLAYER, "Remnant allied fleet");
        api.setFleetTagline(FleetSide.ENEMY, "Templar Final Crusade");

        api.addBriefingItem("Survive");

        api.setHyperspaceMode(true);

        api.addToFleet(FleetSide.PLAYER, "onslaught_xiv_Elite", FleetMemberType.SHIP, "SS Ares", true);
        api.addToFleet(FleetSide.PLAYER, "aurora_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "doom_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "eagle_xiv_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "eagle_xiv_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "gryphon_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "heron_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "heron_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "dominator_XIV_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "sunder_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "sunder_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Balanced", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_XIV_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_XIV_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "enforcer_XIV_Elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "afflictor_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "shade_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "omen_PD", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "monitor_Escort", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "lasher_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "lasher_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "lasher_PD", FleetMemberType.SHIP, false);

        api.addToFleet(FleetSide.ENEMY, "tem_archbishop_mit", FleetMemberType.SHIP, "Roland", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_paladin_est", FleetMemberType.SHIP, "Gerin", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_paladin_mit", FleetMemberType.SHIP, "Anseis", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_chevalier_sal", FleetMemberType.SHIP, "Yvon", false).getCaptain().setPersonality(
                "cautious");
        api.addToFleet(FleetSide.ENEMY, "tem_crusader_sal", FleetMemberType.SHIP, "Engeler", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_crusader_agi", FleetMemberType.SHIP, "Gerer", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_crusader_ati", FleetMemberType.SHIP, "Berenger", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_est", FleetMemberType.SHIP, "Samson", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_sal", FleetMemberType.SHIP, "Oton", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_def", FleetMemberType.SHIP, "Gerard", false).getCaptain().setPersonality(
                "aggressive");
        api.addToFleet(FleetSide.ENEMY, "tem_jesuit_ati", FleetMemberType.SHIP, "Yvoire", false).getCaptain().setPersonality(
                "aggressive");

        float width = 25000f;
        float height = 35000f;
        api.initMap(-width / 2f, width / 2f, -height / 2f, height / 2f);

        for (int i = 0; i < 30; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 250f + (float) Math.random() * 1500f;
            api.addNebula(x, y, radius);
        }

        api.addNebula(width * 0.1f, -height * 0.05f, 4000);
        api.addNebula(-width * 0.1f, -height * 0.05f, 4000);
        api.addNebula(0, height * 0.05f, 4000);

        float minX = -width / 2;
        float minY = -height / 2;

        int minAsteroidSpeed = 40;
        int asteroidCount = 250 + (int) (100.0 * Math.pow(Math.random(), 2.0));

        api.addAsteroidField(minX + width * 0.5f,
                             minY + height * 0.5f,
                             rand.nextInt(90) - 45 + (rand.nextInt() % 2) * 180,
                             100 + (int) (Math.random() * height / 2),
                             minAsteroidSpeed,
                             minAsteroidSpeed * 1.1f,
                             asteroidCount);

        api.addObjective(width * 0.4f, -height * 0.05f, "nav_buoy");
        api.addObjective(-width * 0.4f, -height * 0.05f, "nav_buoy");
        api.addObjective(width * 0.15f, height * 0.4f, "comm_relay");
        api.addObjective(-width * 0.15f, height * 0.4f, "comm_relay");
        api.addObjective(0, 0, "comm_relay");
        api.addObjective(width * 0.25f, -height * 0.3f, "sensor_array");
        api.addObjective(-width * 0.25f, -height * 0.3f, "sensor_array");
    }
}
